---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.1.2-pilot|
| Versie | 1.1.2-pilot vaststelling door de Ketenraad KIK-V |
| Doel | Release 1.1.2-pilot betreft de basisinformatie die benodigd is voor kostenonderzoek door de Nederlandse Zorgautoriteit. |
| Doelgroep | Nederlandse Zorgautoriteit Zorgaanbieders verpleeghuiszorg |
| Totstandkoming | De ontwikkeling van release 1.1.2-pilot is uitgevoerd door het programma KIK-V in samenwerking met de NZa. Release 1.1.2-pilot wordt vastgesteld door de Ketenraad KIK-V op basis van versie 0.9. |
| Operationeel toepassingsgebied | Ter voorbereiding op mogelijk kostenonderzoek in de toekomst. Omdat nog niet duidelijk is of en wanneer er een (kosten)onderzoek gaat plaatsvinden, gaat het hier om een concept voor een mogelijk toekomstig uitwisselprofiel. |
| Status | Voor advies aan de Ketenraad voorgelegd aan het Tactisch Overleg |
| Functionele scope | Het uitwisselprofiel bevat de afspraken over de uitwisseling van gegevens op alle basisinformatie ten behoeve van kostenonderzoek. Deze basisinformatie is gebaseerd op eerder kostenonderzoek waaronder het onderzoek dat in 2016 is uitgevoerd. |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |
