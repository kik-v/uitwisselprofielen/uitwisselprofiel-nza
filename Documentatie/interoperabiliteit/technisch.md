---
title: Technische interoperabiliteit
weight: 4
---
*  Uitwisseling via datastations verloopt conform [Afspraken uitwisseling](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/afspraken_gegevensuitwisseling_datastations/).

**Let op:** Afspraken over de werking van de voorzieningen zijn over het algemeen onderdeel van de documentatie van de gebruikte voorzieningen voor gegevensuitwisseling zelf. Het uitwisselprofiel verwijst naar de relevante documentatie.

Indien een zorgaanbieder indicatoren kan berekenen en aanleveren via een datastation, dan kunnen die worden aangeleverd middels de KIK-Starter. Voor meer informatie hierover zie [het Publicatieplatform](https://kik-v-publicatieplatform.nl/). 

Voor aanlevering middels de KIK-Starter gelden de volgende afspraken:

* Zorgaanbieder kunnen zich aanmelden om gebruik te maken van de KIK-Starter. Deze zorgaanbieders krijgen van het programma KIK-V toegang tot de KIK-Starter via een daarvoor ingericht aansluitproces.
* Voor gebruik van de KIK-Starter worden geen kosten in rekening gebracht.
* Herstel/correctie is mogelijk tot het moment van het verwerken van de uitkomsten van het kostenonderzoek. Na dit moment alleen in onderling overleg. Over de wens tot herstel/correctie is onderling contact tussen het programma KIK-V en de NZa.