def indicator_nza_9_3(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal energiekosten kosten (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id in [
                "WBedHuiGwe",
                "WBedHuiGas",
                "WBedHuiWat",
                "WBedHuiElk",
                "WBedHuiTrg",
                "473000",
                "473100",
                "473200",
                "473300",
                "473400",
                "473500",
                "473600",
                "473900",
            ]:
                indicator["Totaal"]["Totaal energiekosten kosten (Euro)"] += post.price

    output_string = f"\nNZa 9.3 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f"Totaal energiekosten kosten (Euro) \
    {round(indicator['Totaal']['Totaal energiekosten kosten (Euro)'], 2)}"
    return output_string