def indicator_nza_12_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal Toegekend wettelijk budget (euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:3] == "811":
                indicator["Totaal"][
                    "Totaal Toegekend wettelijk budget (euro)"
                ] += post.price

    output_string = f"\nNZa 12.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f"Totaal Toegekend wettelijk budget (euro) \
    {round(indicator['Totaal']['Totaal Toegekend wettelijk budget (euro)'], 2)}"
    return output_string