def indicator_nza_3_1(start_periode, eind_periode, test_instances):
    output_string = f'\nNZA 3.1 Overige personeelskosten van personeel in loondienst (PIL): {start_periode}, eind_periode: {eind_periode}'
    totaal = 0
    rubrieken = {
        "WPerSol",
        "WPerLes",
        "411000",
        "411100",
        "411200",
        "411300",
        "411400",
        "411500",
        "411600",
        "411700",
        "412000",
        "412100",
        "412200",
        "412300",
        "412400",
        "412500",
        "413000",
        "413100",
        "413200",
        "413300",
        "413400",
        "413500",
        "413600",
        "413610",
        "413620",
        "413700",
        "414000",
        "414100",
        "414101",
        "414102",
        "414103",
        "414104",
        "414110",
        "414111",
        "414112",
        "414113",
        "414114",
        "414200",
        "414202",
        "414203",
        "414204",
        "414300",
        "414500",
        "414503",
        "414504",
        "414700",
        "414800",
        "415000",
        "416000",
        "417000",
        "419000",
        "419100",
        "419101",
        "419200",
        "419500",
        "420000",
        "420100",
        "422100",
        "422300",
        "422400",
        "422410",
        "422500",
        "422600",
        "422900"
    }
    rubrieken = expand_rubrieken(rubrieken)  # expand list with children rubrieken
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken:
                totaal += post.price
    
    output_string += f'\n    Totaal organisatie: {round(totaal, 2)}'
    return output_string

# file.write(indicator_nza_3_1(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
