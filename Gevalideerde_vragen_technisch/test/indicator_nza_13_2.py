def indicator_nza_13_2(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal eigen bijdragen cliënten (euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:3] == "822":
                indicator["Totaal"][
                    "Totaal eigen bijdragen cliënten (euro)"
                ] += post.price

    output_string = f"\nNZa 13.2 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f"Totaal eigen bijdragen cliënten (euro) \
    {round(indicator['Totaal']['Totaal eigen bijdragen cliënten (euro)'], 2)}"
    return output_string