def indicator_nza_6_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal vervoerskosten (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:3] == "443":
                if rubriek_id[3:6] in ["000", "100", "200", "300"]:
                    indicator["Totaal"]["Totaal vervoerskosten (Euro)"] += post.price

    output_string = f"\nNZa 6.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f' Totaal vervoerskosten (Euro) \
        {round(indicator["Totaal"]["Totaal vervoerskosten (Euro)"], 2)}'
    return output_string
