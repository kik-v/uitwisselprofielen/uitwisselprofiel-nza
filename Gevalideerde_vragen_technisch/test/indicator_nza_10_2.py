def indicator_nza_10_2(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"][
        "Totaal aan kosten voor vervoersmiddelen en immateriele vaste activa (Euro)"
    ] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:6] in [
                "480012",
                "480022",
                "480032",
                "480112",
                "480122",
                "480132",
                "480532",
                "480632",
                "480932",
                "484222",
                "484232",
                "486725",
                "486735",
            ]:

                indicator["Totaal"][
                    "Totaal aan kosten voor vervoersmiddelen en immateriele vaste activa (Euro)"
                ] += post.price

    output_string = f"\nNZa 10.2 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f"Totaal aan kosten voor vervoersmiddelen en immateriele vaste activa (Euro) \
    {round(indicator['Totaal']['Totaal aan kosten voor vervoersmiddelen en immateriele vaste activa (Euro)'], 2)}"
    return output_string