def indicator_nza_4_3(start_periode, eind_periode, test_instances):
    output_string = f'\nNZA 4.3. Overige personeelskosten van personeel in loondienst (PIL): {start_periode}, eind_periode: {eind_periode}'
    totaal = 0
    rubrieken = {
        "WBedOvpWer",
        "WBedOvpAbd",
        "WBedOvpDdd",
        "WBedOvpZie",
        "WBedOvpOzi",
        "WBedOvpDvr",
        "WBedOvpVvr",
        "WBedOvpDoa",
        "WBedOvpDoj",
        "WBedOvpVva",
        "WBedOvpVrj",
        "WBedOvpObp",
        "WBedOvpOvp",
        "WBedOvpDop",
        "WBedOvpLbo",
        
        "423100",
        "423110",
        "423120",
        "423140",
        "423190",
        "423200",
        "423210",
        "423220",
        "423300",
        "423400",
        "423410",
        "423420",
        "423500",
        "423510",
        "423520",
        "423600",
        "423700",
        "423900",
        "423910",
        "423920"
    }
    rubrieken = expand_rubrieken(rubrieken)  # expand list with children rubrieken
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken:
                totaal += post.price
    
    output_string += f'\n    Totaal organisatie: {round(totaal, 2)}'
    return output_string

# file.write(indicator_nza_4_3(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
