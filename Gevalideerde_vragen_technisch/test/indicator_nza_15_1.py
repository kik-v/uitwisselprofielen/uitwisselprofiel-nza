def indicator_nza_15_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal Opbrengsten uit gewone bedrijfsuitoefening (euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id in [
                "811000",
                "821000",
                "822000",
                "825000",
                "826000",
                "827000",
                "828000",
                "829000",
                "831000",
                "832000",
                "833000",
                "835000",
            ]:
                indicator["Totaal"][
                    "Totaal Opbrengsten uit gewone bedrijfsuitoefening (euro)"
                ] += post.price

    output_string = f"\nNZa 15.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f'Totaal Opbrengsten uit gewone bedrijfsuitoefening (euro) \
    {round(indicator["Totaal"]["Totaal Opbrengsten uit gewone bedrijfsuitoefening (euro)"], 2)}'
    return output_string