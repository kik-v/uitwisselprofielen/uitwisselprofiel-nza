def indicator_nza_1_1(start_periode, eind_periode, mensen):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Ziekteverzuimpercentage"] = {"teller": 0, "noemer": 0}
    periode = (start_periode, eind_periode)
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for (werkovereenkomstafspraak) in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36  # omvang in fte
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator["Totaal"]["Ziekteverzuimpercentage"]["noemer"] += dagen_afspraak * ptf
                        for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                            verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                            # Ziekte excl. Zwangerschap
                            if verzuim_periode and verzuim.get_type() == onzpers.ZiektePeriode:
                                totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                hersteld = 0
                                verzuim_corr = days_in_period(verzuim_periode)
                                for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                    verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                    if verzuimtijd_periode:
                                        verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                        vp = verzuimtijd.get_ziekte_percentage() / 100
                                        hersteld += verzuimtijd_dagen * (1-vp)
                                indicator["Totaal"]["Ziekteverzuimpercentage"]["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nNZa 1.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:\n"

    percentage = (100 * indicator["Totaal"]["Ziekteverzuimpercentage"]["teller"] / indicator["Totaal"]["Ziekteverzuimpercentage"]["noemer"]
        if indicator["Totaal"]["Ziekteverzuimpercentage"]["noemer"] != 0
        else "Ongedefinieerd")
    
    output_string += "\tteller: " + str(indicator["Totaal"]["Ziekteverzuimpercentage"]["teller"])
    output_string += ", noemer: " + str(indicator["Totaal"]["Ziekteverzuimpercentage"]["noemer"])
    output_string += ", indicator: " + str(percentage)
    
    return output_string