def indicator_nza_5_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal kosten voeding (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:2] == "43":
                if rubriek_id[2:6] in ["1000", "1100", "1200", "1300", "2000", "3000"]:
                    indicator["Totaal"]["Totaal kosten voeding (Euro)"] += post.price

    output_string = f"\nNZa 5.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f' Totaal kosten voeding (Euro) \
        {round(indicator["Totaal"]["Totaal kosten voeding (Euro)"], 2)}'
    return output_string
