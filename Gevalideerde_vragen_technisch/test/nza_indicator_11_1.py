def indicator_nza_11_1(start_periode, eind_periode, test_instances):
    output_string = f'\nNZA 11.1 Wat zijn de kosten en opbrengsten voor hulpbedrijven en overboekingsrekening?: {start_periode}, eind_periode: {eind_periode}'
    totaal = 0
    rubrieken = {
        "491000", 
        "499000"
    }
    rubrieken = expand_rubrieken(rubrieken)  # expand list with children rubrieken
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken:
                totaal += post.price
    
    output_string += f'\n    Totaal organisatie: {round(totaal, 2)}'
    return output_string

# file.write(indicator_nza_11_1(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
