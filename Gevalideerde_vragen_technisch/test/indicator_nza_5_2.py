def indicator_nza_5_2(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal andere hotelmatige kosten (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:2] == "44":
                if rubriek_id[2] == "1":
                    if rubriek_id[3:6] in ["000", "100", "200", "300", "400", "900"]:
                        indicator["Totaal"][
                            "Totaal andere hotelmatige kosten (Euro)"
                        ] += post.price
                elif rubriek_id[2:6] in [
                    "2000",
                    "2100",
                    "2200",
                    "4000",
                    "4100",
                    "5000",
                    "5100",
                    "5200",
                    "9000",
                ]:
                    indicator["Totaal"][
                        "Totaal andere hotelmatige kosten (Euro)"
                    ] += post.price

    output_string = f"\nNZa 5.2 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f' Totaal andere hotelmatige kosten (Euro) \
        {round(indicator["Totaal"]["Totaal andere hotelmatige kosten (Euro)"], 2)}'
    return output_string
