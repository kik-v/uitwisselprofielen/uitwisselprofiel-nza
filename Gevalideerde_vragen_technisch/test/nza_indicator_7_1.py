def indicator_nza_7_1(start_periode, eind_periode, test_instances):
    output_string = f'\nNZA 7.1 Wat zijn de algemene kosten?: {start_periode}, eind_periode: {eind_periode}'
    totaal = 0
    rubrieken = {
        "451000",
        "451100",
        "451200",
        "451300",
        "451400",
        "451500",
        "451600",
        "452000",
        "452100",
        "452200",
        "452900",
        "453000",
        "453100",
        "453200",
        "453300",
        "453900",
        "453910",
        "453920",
        "453930",
        "453990",
        "454000",
        "454100",
        "454200",
        "454210",
        "454220",
        "454290",
        "455000",
        "455100",
        "459000",
        "459100",
        "459300",
        "459900"
    }
    rubrieken = expand_rubrieken(rubrieken)  # expand list with children rubrieken
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken:
                totaal += post.price
    
    output_string += f'\n    Totaal organisatie: {round(totaal, 2)}'
    return output_string

# file.write(indicator_nza_7_1(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
