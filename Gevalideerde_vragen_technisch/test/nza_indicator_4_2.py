def indicator_nza_4_2(start_periode, eind_periode, test_instances):
    output_string = f'\nNZA 4.2. Kosten personeel niet in loondienst (PNIL): {start_periode}, eind_periode: {eind_periode}'
    totaal = 0
    rubrieken = {
        "WBedOvpUik",
        "WBedOvpUikUik",
        "WBedOvpUikFor",
        "WBedOvpUikPrj",
        "WBedOvpUikBfo",
        "WBedOvpUikPro",
        "WBedOvpUit",
        "WBedOvpMaf",
        "WBedOvpZzp",
        "WBedOvpPay",
        "WBedOvpOip",
        "418000",
        "418100",
        "418200"
    }
    rubrieken = expand_rubrieken(rubrieken)  # expand list with children rubrieken
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken:
                totaal += post.price
    
    output_string += f'\n    Totaal organisatie: {round(totaal, 2)}'
    return output_string

# file.write(indicator_nza_4_2(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
