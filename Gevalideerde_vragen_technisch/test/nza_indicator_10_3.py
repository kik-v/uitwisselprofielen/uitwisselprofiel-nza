def indicator_nza_10_3(start_periode, eind_periode, test_instances):
    output_string = f'\nNZA 10.3 Wat zijn huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties?: {start_periode}, eind_periode: {eind_periode}'
    totaal = 0
    rubrieken = {
        "481212",
        "481222",
        "481232",
        "481242",
        "481252",
        "482112",
        "482122",
        "482132",
        "482142",
        "482152",
        "482212",
        "482222",
        "482232",
        "482242",
        "482252",
        "482312",
        "482322",
        "482332",
        "482342",
        "482352",
        "482412",
        "482422",
        "482432",
        "482442",
        "482452",
        "483112",
        "483122",
        "483132",
        "483142",
        "483152",
        "484112",
        "484122",
        "484132",
        "484222",
        "484232",
        "484322",
        "484332",
        "485000",
        "485100",
        "485190",
        "485205",
        "485211",
        "485300",
        "485311",
        "485412",
        "485900",
        "486000",
        "486115",
        "486125",
        "486135",
        "486215",
        "486225",
        "486235",
        "486315",
        "486325",
        "486335",
        "486415",
        "486425",
        "486435",
        "486515",
        "486525",
        "486535",
        "486615",
        "486625",
        "486635",
        "486825",
        "486835",
        "489150",
        "489200",
        "489211",
        "489212",
        "489221",
        "489222",
        "489230"
    }
    rubrieken = expand_rubrieken(rubrieken)  # expand list with children rubrieken
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken:
                totaal += post.price
    
    output_string += f'\n    Totaal organisatie: {round(totaal, 2)}'
    return output_string

# file.write(indicator_nza_10_3(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
