def indicator_nza_8_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal cliënt cq bewonersgebonden kosten (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:2] == "46":
                if rubriek_id[2:6] in [
                    "1000",
                    "1100",
                    "1200",
                    "2000",
                    "2100",
                    "2200",
                    "2300",
                    "2400",
                    "2500",
                    "2600",
                    "2700",
                    "2800",
                    "2900",
                    "3000",
                    "3100",
                    "3200",
                    "3900",
                    "4000",
                    "4100",
                    "4200",
                    "4300",
                    "4900",
                    "5000",
                    "5100",
                    "5200",
                    "5300",
                    "5400",
                    "5900",
                    "6000",
                    "7100",
                    "7110",
                    "7120",
                    "7200",
                    "8000",
                ]:
                    indicator["Totaal"][
                        "Totaal cliënt cq bewonersgebonden kosten (Euro)"
                    ] += post.price

    output_string = f"\nNZa 8.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f' Totaal cliënt cq bewonersgebonden kosten (Euro) \
        {round(indicator["Totaal"]["Totaal cliënt cq bewonersgebonden kosten (Euro)"], 2)}'
    return output_string
