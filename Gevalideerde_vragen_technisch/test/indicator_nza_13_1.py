def indicator_nza_13_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal Overige opbrengsten (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:3] in [
                "821",
                "825",
                "826",
                "827",
                "828",
                "829",
                "831",
                "832",
                "833",
                "835",
            ]:
                indicator["Totaal"]["Totaal Overige opbrengsten (Euro)"] += post.price

    output_string = f"\nNZa 13.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f"Totaal Overige opbrengsten (Euro) \
    {round(indicator['Totaal']['Totaal Overige opbrengsten (Euro)'], 2)}"
    return output_string