def indicator_nza_3_2(start_periode, eind_periode, test_instances, mensen):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal bruto jaarsalaris"] = 0
    indicator["Totaal"]["Totaal aantal contractuele fte"] = 0
    indicator["Totaal"]["Gemiddelde bruto jaarsalaris per fte"] = 0

    # Totaal bruto jaarsalaris
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[0:3] == "422":
                if rubriek_id[3:6] in [
                    "100",
                    "300",
                    "400",
                    "410",
                    "500",
                    "600",
                    "900",
                ]:
                    indicator["Totaal"]["Totaal bruto jaarsalaris"] += post.price

            elif rubriek_id[0:2] == "41":
                if rubriek_id[2:6] in [
                    "1000",
                    "1100",
                    "1200",
                    "1300",
                    "1400",
                    "1500",
                    "1600",
                    "1700",
                    "2000",
                    "2100",
                    "2200",
                    "2300",
                    "2400",
                    "2500",
                    "3000",
                    "3100",
                    "3200",
                    "3300",
                    "3400",
                    "3500",
                    "3600",
                    "3610",
                    "3620",
                    "3700",
                    "4000",
                    "4100",
                    "4101",
                    "4102",
                    "4103",
                    "4104",
                    "4110",
                    "4111",
                    "4112",
                    "4113",
                    "4114",
                    "4200",
                    "4202",
                    "4203",
                    "4204",
                    "4300",
                    "4500",
                    "4503",
                    "4504",
                    "4700",
                    "4800",
                    "5000",
                ]:
                    indicator["Totaal"]["Totaal bruto jaarsalaris"] += post.price
            elif rubriek_id[:7] in ["WPerSol", "WPerLes"]:
                indicator["Totaal"]["Totaal bruto jaarsalaris"] += post.price

    # Totaal aantal contractuele fte
    periode = (start_periode, eind_periode)
    for mens in mensen:
        ptfs = []
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36  # omvang in fte
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator["Totaal"]["Totaal aantal contractuele fte"] += (dagen_afspraak/days_in_period(periode)) * ptf

    # Gemiddelde bruto jaarsalaris per fte
    indicator["Totaal"]["Gemiddelde bruto jaarsalaris per fte"] = (
        indicator["Totaal"]["Totaal bruto jaarsalaris"]
        / indicator["Totaal"]["Totaal aantal contractuele fte"]
        if indicator["Totaal"]["Totaal aantal contractuele fte"] > 0
        else "Ongedefinieerd"
    )

    output_string = f"\nNZa 3.2 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f'\n Totaal bruto jaarsalaris: {round(indicator["Totaal"]["Totaal bruto jaarsalaris"], 2)}'
    output_string += f'\n Totaal aantal contractuele fte: {round(indicator["Totaal"]["Totaal aantal contractuele fte"], 2)}'
    output_string += f'\n Gemiddelde bruto jaarsalaris per fte: {round(indicator["Totaal"]["Gemiddelde bruto jaarsalaris per fte"], 2)}'
    return output_string
