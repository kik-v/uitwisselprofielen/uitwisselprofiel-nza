def indicator_nza_4_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal sociale kosten PIL (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:7] == "WPerSol":
                indicator["Totaal"]["Totaal sociale kosten PIL (Euro)"] += post.price
            elif rubriek_id[:3] == "422":
                if rubriek_id[3:6] in ["100", "300", "400", "410", "500", "600", "900"]:
                    indicator["Totaal"][
                        "Totaal sociale kosten PIL (Euro)"
                    ] += post.price

    output_string = f"\nNZa 4.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f' Totaal sociale kosten PIL (Euro) \
        {round(indicator["Totaal"]["Totaal sociale kosten PIL (Euro)"], 2)}'
    return output_string
