def indicator_nza_9_2(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"][
        "Totaal onderhoudskosten en dotaties aan voorzieningen groot onderhoud (Euro)"
    ] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[:6] == "472000":
                indicator["Totaal"][
                    "Totaal onderhoudskosten en dotaties aan voorzieningen groot onderhoud (Euro)"
                ] += post.price

            elif rubriek_id == "WBedHuiDvg":
                indicator["Totaal"][
                    "Totaal onderhoudskosten en dotaties aan voorzieningen groot onderhoud (Euro)"
                ] += post.price

    output_string = f"\nNZa 9.2 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f"Totaal onderhoudskosten en dotaties aan voorzieningen groot onderhoud (Euro) \
    {round(indicator['Totaal']['Totaal onderhoudskosten en dotaties aan voorzieningen groot onderhoud (Euro)'], 2)}"
    return output_string