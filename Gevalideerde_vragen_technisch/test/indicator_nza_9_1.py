def indicator_nza_9_1(start_periode, eind_periode, test_instances):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["Totaal terrein- en gebouwgebonden kosten (Euro)"] = 0

    for post in [
        instance for instance in test_instances if isinstance(instance, Grootboekpost)
    ]:
        if start_periode <= post.datum <= eind_periode:
            rubriek_id = str(post.rubriek).split("onz-fin#")[1]

            if rubriek_id[0:7] == "WBedEem" or rubriek_id[0:10] in [
                "WBedHuiOnt",
                "WBedHuiOng",
            ]:
                indicator["Totaal"][
                    "Totaal terrein- en gebouwgebonden kosten (Euro)"
                ] += post.price
            elif rubriek_id[:3] == "471":
                if rubriek_id[3:6] in ["100", "200", "300", "400"]:
                    indicator["Totaal"][
                        "Totaal terrein- en gebouwgebonden kosten (Euro)"
                    ] += post.price

    output_string = f"\nNZa 9.1 indicator start_periode: {start_periode}, eind_periode: {eind_periode}:"
    output_string += "\n Totaal organisatie:"
    output_string += f' Totaal terrein- en gebouwgebonden kosten (Euro) \
        {round(indicator["Totaal"]["Totaal terrein- en gebouwgebonden kosten (Euro)"], 2)}'
    return output_string
