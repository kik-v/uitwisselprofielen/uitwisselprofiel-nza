
#  Testcases Overzicht NZa basisinformatie t.b.v kostenonderzoek
---

Hieronder staat een overzicht van de status van de testautomatisering voor NZa basisinformatie t.b.v kostenonderzoek

## Status ##

| Indicator           | Specificatie | Data         | Testautomatisering | Opmerking |
|---------------------|--------------|--------------|----------------|-----------|
| Indicator 1.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 2.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 3.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 3.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 3.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 4.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 4.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 4.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 5.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 5.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 6.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 7.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 8.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 9.1.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 9.2.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 9.3.md   | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 10.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 10.2.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 10.3.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 11.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 12.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 12.2.md  | n.v.t. | n.v.t. |  n.v.t.  |   Indicator is vervallen  |
| Indicator 13.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 13.2.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 14.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |
| Indicator 15.1.md  | {+ Gereed +} | {+ Gereed +} |  {+ Gereed +}  |   n.v.t.  |