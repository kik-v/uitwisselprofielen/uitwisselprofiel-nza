# Testcases NZa basisinformatie t.b.v. kostenonderzoek 4.2. Wat zijn de PNIL kosten?

---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 4.2. Wat zijn de PNIL kosten


## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken PNIL kosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 02 | td_02 | Matching rubrieken PNIL kosten: N (Uitzendkracht kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 03 | td_03 | Matching rubrieken PNIL kosten: J (Sociale lasten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 04 | td_04 | Matching rubrieken PNIL kosten: J (Uitzendkrachten - uitzendkrachten, formatief, projectmatig, boven formatief, programma's) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 04_a | td_04_a | Matching rubrieken PNIL kosten: J (Uitzendbedrijven, management fee, ingehuurde ZZP-ers, Ingehuurde payrollerss, Overig ingeleend personeel) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 04_b | td_04_b | Matching rubrieken PNIL kosten: J (Prizmant 4180, 4181 & 4182) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt  |