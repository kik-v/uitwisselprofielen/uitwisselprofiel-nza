# Testcases NZa basisinformatie t.b.v. kostenonderzoek 7.1. Wat zijn de algemene kosten?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 7.1. Wat zijn de algemene kosten?

## Testcases overview table ##


| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken algemene kosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Matching rubrieken algemene kosten: N (Sociale kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 03 | td_03 | Matching rubrieken algemene kosten: J (451100) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Matching rubrieken algemene kosten: J (Prizmant 451000, -1100, -1200, -1300, -1400, -1500, -1600, -2100 & 452200) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04_a | td_04_a | Matching rubrieken algemene kosten: J (Prizmant 452900, -3100, -3200, -3300, -3910, -3920, -3930 & 453990) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04_b | td_04_b | Matching rubrieken algemene kosten: J (Prizmant 454100, -4210, -4220, -4290, -5100, -9100, -9300 & 459900) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
