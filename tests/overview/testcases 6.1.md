# Testcases NZa basisinformatie t.b.v. kostenonderzoek 6.1. Wat zijn de vervoerskosten?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 6.1. Wat zijn de vervoerskosten?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken vervoers kosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Matching rubrieken vervoers kosten: N (Sociale kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 03 | td_03 | Matching rubrieken vervoers kosten: J (443100) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Matching rubrieken vervoers kosten: J (Prizmant 443100, -3200, -3300, 443000) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
