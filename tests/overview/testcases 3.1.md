# Testcases NZa basisinformatie t.b.v. kostenonderzoek 3.1. Wat zijn de loonkosten personeel in loondienst?

---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 3.1. Wat zijn de loonkosten personeel in loondienst?


## Testcases overview table ##


| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken PIL: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | nvt |
| 02 | td_02 | Matching rubrieken PIL: N (Uitzendkracht kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | nvt |
| 03 | td_03 | Matching rubrieken PIL: J (Sociale lasten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | nvt |
| 04 | td_04 | Matching rubrieken PIL: J (Loon & Salaris + Sociale lasten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | nvt |
| 04_a | td_04_a | Matching rubrieken PIL: J (Prizmant 411 t/m 413) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | nvt |
| 04_b | td_04_b | Matching rubrieken PIL: J (Prizmant 414 t/m 415) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 | nvt |
