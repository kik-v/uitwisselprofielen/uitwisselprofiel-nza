# Testcases NZa basisinformatie t.b.v. kostenonderzoek 10.2. Wat zijn de kosten voor vervoersmiddelen en immateriele vaste activa?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 10.2. Wat zijn de kosten voor vervoersmiddelen en immateriele vaste activa?

## Testcases overview table ##


| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | d_01 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | ZZP kosten, Valt niet in meetperiode |
| 02 | td_02 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Sociale kosten, Valt wel in meetperiode |
| 03 | td_03 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prismant 480012, Valt niet in meetperiode |
| 04 | td_04 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prismant 480012, -022, -032, -112, -122, -132, -532, -632, -932, 484222, -232, 486725 & 486735, Valt wel in meetperiode |
| 04a | td_04_a | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prismant 480012, -022, -032, -112, -122, -132, -532, -632, -932, 484222, -232, 486725 & 486735, Valt wel in meetperiode |
