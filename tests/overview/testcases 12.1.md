# Testcases NZa basisinformatie t.b.v kostenonderzoek 11.1. Wat zijn de kosten en opbrengsten voor hulpbedrijven en overboekingsrekening?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v kostenonderzoek 11.1. Wat zijn de kosten en opbrengsten voor hulpbedrijven en overboekingsrekening?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | **Geen matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **ZZP kosten, Valt niet in meetperiode** |
| 02 | td_02 | **Geen matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **Sociale kosten, Valt wel in meetperiode** |
| 03 | td_03 | **Wel matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **811000, Valt niet in meetperiode** |
| 04 | td_04 | **Wel matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **811000, Valt wel in meetperiode** |