# Testcases NZa basisinformatie t.b.v. kostenonderzoek 9.1. Wat zijn de terrein- en gebouwgebonden kosten?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 9.1. Wat zijn de terrein- en gebouwgebonden kosten?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | ZZP kosten, Valt niet in meetperiode |
| 02 | td_02 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Uitzendkracht kosten, Valt wel in meetperiode |
| 03 | td_03 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Onderhoud terrein, Valt niet in meetperiode |
| 04 | td_04 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Onderhoud terrein & gebouw & exploitatie & machinekosten, Valt wel in meetperiode |
| 04a | td_04_a | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prizmant 471100, -1200, -1300 & 471400, Valt wel in meetperiode |
