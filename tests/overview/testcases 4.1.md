# Testcases NZa basisinformatie t.b.v. kostenonderzoek 4.1. Wat zijn de sociale kosten?

---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 4.1. Wat zijn de sociale kosten?


## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken sociale kosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Matching rubrieken sociale kosten: N (Uitzendkracht kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 03 | td_03 | Matching rubrieken sociale kosten: J (Sociale lasten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Matching rubrieken sociale kosten: J (Sociale lasten, Premies sociale verzekeringen, Bijdrage ziektekostenverzekering, Overige premies, Overige sociale fondsen, Overige sociale lasten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04_a | td_04_a | Matching rubrieken sociale kosten: J (Prizmant 4221, -23, -24, -241, -25, -26 & 4229) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
