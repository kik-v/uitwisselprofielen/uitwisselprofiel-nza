# Testcases NZa basisinformatie t.b.v kostenonderzoek 13.1. Wat zijn de overige bedrijfsopbrengsten? 
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v kostenonderzoek 13.1. Wat zijn de overige bedrijfsopbrengsten? 

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | **Geen matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **ZZP kosten, Valt niet in meetperiode** |
| 02 | td_02 | **Geen matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **Sociale kosten, Valt wel in meetperiode** |
| 03 | td_03 | **Wel matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **821000, Valt niet in meetperiode** |
| 04 | td_04 | **Wel matching rubrieken** | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | **821000, 825000, 826000, 827000, 828000, 829000, 831000, 832000, 833000 & 835000, Valt wel in meetperiode** |