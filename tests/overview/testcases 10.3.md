# Testcases NZa basisinformatie t.b.v. kostenonderzoek 10.3. Wat zijn huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 10.3. Wat zijn huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | ZZP kosten, Valt niet in meetperiode |
| 02 | td_02 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Sociale kosten, Valt wel in meetperiode |
| 03 | td_03 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | 481212, Valt niet in meetperiode |
| 04 | td_04 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prizmant 481212, -1222, -1232, -1242, -1252, 482112, -2122, -2132, -2142, -2152, -2212, -2222, -2232, -2242, -2252, -2312, -2322, -2332, -2342, -2352, Valt wel in meetperiode |
| 04a | td_04_a | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prizmant 463000, -3100, -3200, -3900, -4000, -4100, -4200, -4300 & 464900, Valt wel in meetperiode |
| 04b | td_04_b | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Huur terreinvoorzieningen, gebouwen, semi-permanente gebouwen, houten paviljoens, Valt wel in meetperiode |
| 04c | td_04_c | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Huur installaties, medische inventaris, inventaris, vervoersmiddelen & automatisering, dotaties, Valt wel in meetperiode |
| 04d | td_04_d | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Afschrijvingskosten terreinvoorzieiningen - immateriele vaste activata, medische inventaris etc. & huur medische inventaris etc., Valt wel in meetperiode |
