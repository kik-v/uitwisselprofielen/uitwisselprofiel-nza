# Testcases NZa basisinformatie t.b.v kostenonderzoek 1.1. Wat is het ziekteverzuimpercentage (excl. zwangerschapsverlof)?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v kostenonderzoek 1.1. Wat is het ziekteverzuimpercentage (excl. zwangerschapsverlof)?

## Testcases overview table ##


| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: N (36u p/w) <br> Verzuim: N <br> Zwangerschaps-bevallingsverlof: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Heeft ZVL functie: J <br> Heeft AOK: N <br> Parttimefactor: N (36u p/w) <br> Verzuim: N <br> Zwangerschaps-bevallingsverlof: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: N <br> Verzuim: N <br> Zwangerschaps-bevallingsverlof: N <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05 | td_05 | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: N <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: N <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05_a | td_05_a | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: J (18u p/w) <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: N <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05_b | td_05_b | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: N <br> Verzuim: N <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05_c | td_05_c | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: J (18u p/w) <br> Verzuim: N <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05_d | td_05_d | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: N <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05_e | td_05_e | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: J (18u p/w) <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 05_f | td_05_f | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: N <br> Verzuim: J (6 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 06 | td_06 | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: J (18u p/w) <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 06_a | td_06_a | Heeft ZVL functie: J <br> Heeft AOK: J <br> Parttimefactor: N <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 07 | td_07 | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: J (18u p/w) <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 07_a | td_07_a | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: N <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: N <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 08 | td_08 | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: J (18u p/w) <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 08_a | td_08_a | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: J (18u p/w) <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: N <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 08_b | td_08_b | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: J (18u p/w) <br> Verzuim: N <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 08_c | td_08_c | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: N <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: J (3 maanden) <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 08_e | td_08_e | Heeft ZVL functie: N <br> Heeft AOK: N <br> Parttimefactor: N <br> Verzuim: J (2 weken) <br> Zwangerschaps-bevallingsverlof: N <br> PIL: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |