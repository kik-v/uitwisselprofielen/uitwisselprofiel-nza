# Testcases NZa basisinformatie t.b.v. kostenonderzoek 4.3. Wat zijn de overige personeelskosten van personeel in loondienst (PIL)?

---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 4.3. Wat zijn de overige personeelskosten van personeel in loondienst (PIL)?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken overige personeelskosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Matching rubrieken overige personeelskosten: N (Uitzendkracht kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 03 | td_03 | Matching rubrieken overige personeelskosten: J (Sociale lasten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Matching rubrieken overige personeelskosten: J (Wervingskosten, Arbodienst, Diesnten voor derden, Ziekengeldverzekering, Ontangen ziekengelden, dotatie & vrijval voorziening in verband met reorganisaties) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04_a | td_04_a | Matching rubrieken overige personeelskosten: J (Dotatie & Vrijval arbeidsongeschiktheidsvoorziening, Dotatie & Vrijval jubileumvoorziening, Overige belastingen inzake personeel, overige & doorberekende personeelskosten, loopbaanontwikkeling) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04_b | td_04_b | Matching rubrieken overige personeelskosten: J (Prizmant 423100, -110, -120, -140, -190, -200, -210, -220 & -300) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
| 04_c | td_04_c | Matching rubrieken overige personeelskosten: J (Prizmant 423400, -410, -420, -500, -510, -520, -600 & -700) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 |  |
