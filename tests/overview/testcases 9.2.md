# Testcases NZa basisinformatie t.b.v. kostenonderzoek 9.2. Wat zijn de dotaties aan voorzieningen groot onderhoud?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 9.2. Wat zijn de dotaties aan voorzieningen groot onderhoud?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | ZZP kosten, Valt niet in meetperiode |
| 02 | td_02 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Uitzendkracht kosten, Valt wel in meetperiode |
| 03 | td_03 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Dotatie voorziening groot onderhoud gebouwen, Valt niet in meetperiode |
| 04 | td_04 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Dotatie voorziening groot onderhoud gebouwen & Prismant 472000, Valt wel in meetperiode |
