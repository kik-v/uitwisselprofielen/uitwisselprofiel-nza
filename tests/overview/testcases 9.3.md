# Testcases NZa basisinformatie t.b.v. kostenonderzoek 9.3. Wat zijn de energiekosten (Gas-elektriciteit-water)?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 9.3. Wat zijn de energiekosten (Gas-elektriciteit-water)?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
| ------ | ------ | ------ | ------ | ------ |
| 01 | td_01 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | ZZP kosten, Valt niet in meetperiode |
| 02 | td_02 | Geen matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Uitzendkracht kosten, Valt wel in meetperiode |
| 03 | td_03 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Energiekosten gas, Valt niet in meetperiode |
| 04 | td_04 | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Gas, water, elektra (algemeen), energiekosten gas, water, elektra & elektra teruglevering, Valt wel in meetperiode |
| 04a | td_04_a | Wel matching rubrieken | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-04-01 | Prizmant 473000, -3100, -3200, -3300, -3400, -3500, -3600 & 473700, Valt wel in meetperiode |
