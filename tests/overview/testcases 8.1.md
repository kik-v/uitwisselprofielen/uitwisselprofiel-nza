
# Testcases NZa basisinformatie t.b.v. kostenonderzoek 8.1. Wat zijn de client cq bewonersgebonden kosten?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 8.1. Wat zijn de client cq bewonersgebonden kosten?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken client cq gebonden kosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Matching rubrieken client cq gebonden kosten: N (Sociale kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 03 | td_03 | Matching rubrieken client cq gebonden kosten: J (461100) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Matching rubrieken client cq gebonden kosten: J (Prizmant 46, -1000, -1100, -1200, -2000, -2100, -2200, -2300, -2400, -2500, -2600, -2700, -2800 & 462900) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04_a | td_04_a | Matching rubrieken client cq gebonden kosten: J (Prizmant 463000, -3100, -3200, -3900, -4000, -4100, -4200, -4300 & 464900) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04_b | td_04_b | Matching rubrieken client cq gebonden kosten: J (Prizmant 465000, -5100, -5200, -5300, -5300, -5400, -5900, -6000, -7110, -7120, -7200 & 468000) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
