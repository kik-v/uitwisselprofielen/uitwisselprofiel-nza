# Testcases NZa basisinformatie t.b.v. kostenonderzoek 5.1. Wat zijn de kosten van voeding?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 5.1. Wat zijn de kosten van voeding?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variables | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Matching rubrieken voedings kosten: N (ZZP kosten) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 02 | td_02 | Matching rubrieken voedings kosten: N (Sociale kosten) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 03 | td_03 | Matching rubrieken voedings kosten: J (431000) <br> Valt in meetperiode: N | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
| 04 | td_04 | Matching rubrieken voedings kosten: J (Prizmant 431000, -1100, -1200, -1300, -2000 & 433000) <br> Valt in meetperiode: J | Startdatum meetperiode: 2024-01-01. Einddatum meetperiode: 2024-12-31 |  |
