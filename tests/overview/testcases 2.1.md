# Testcases NZa basisinformatie t.b.v. kostenonderzoek 2.1. Wat is het aantal verloonde uren van personeelsleden per jaar per financieringsstroom?
---
Hier staat een overzicht van de Testgevallen voor het uitvoeren van Query validatie van Uitwisselprofiel NZa basisinformatie t.b.v. kostenonderzoek 2.1. Wat is het aantal verloonde uren van personeelsleden per jaar per financieringsstroom?

## Testcases overview table ##

| Testcase | Testdata class | Testcase variabelen | Query parameters | Opmerkingen |
|---|---|---|---|---|
| 01 | td_01 | Heeft ZVL functie: N <br> Heeft AOK: N (StageOvereenkomst) | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 02a | td_02_a | Heeft ZVL functie: N <br> Heeft AOK: J | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 03 | td_03 | Heeft ZVL functie: J <br> Heeft AOK: N (InhuurOvereenkomst) | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 04 | td_04 | Heeft ZVL functie: J <br> Heeft AOK: J (36u Zorg inzet op Vest. 1254) | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt  |
| 04_a | td_04_a | Heeft ZVL functie: J <br> Heeft AOK: J (18u Zorg inzet op Vest. 1254 & 1287) | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |
| 04_b | td_04_b | Heeft ZVL functie: J <br> Heeft AOK: J (18u Zorg & Niet zorg inzet op Vest. 1254) | Startdatum meetperiode: 2024-01-01.  Einddatum meetperiode: 2024-12-31 | nvt |