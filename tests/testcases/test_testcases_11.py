from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:

# Testcases:
#Opmerkingen:
td_template_1 = [
    {
        "Description": "Testcase template 1 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL + 2023)",
        "Amount": 10, # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 2 weken verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31",
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2023-03-01",
                                "end_date": "2023-03-14"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2023-01-01T09:00:00",
                        #         "end_datetime": "2023-01-01T17:00:00"
                        #     }
                        # ]
                    },
                    # {
                    #     "function": [
                    #         {
                    #             "label": "AOK Wel ZVL ",
                    #             "caregiving_role": True,
                    #             "start_date": "2023-01-01",
                    #             "end_date": "2023-12-31",
                    #         }
                    #     ],
                    #     "start_date": "2023-01-01",
                    #     "end_date": "2023-12-31",                        
                    #     "contract_agreement": [
                    #         {
                    #             "start_date": "2023-01-01",
                    #             "end_date": "2023-12-31",
                    #             "location": "Vestiging_Grotestraat",                         
                    #             "size": [
                    #                 {
                    #                     "unit": 36,
                    #                     "unit_of_measure": "Uren_per_week_unit",
                    #                     "start_date": "2023-01-01",
                    #                     "end_date": "2023-12-31"
                    #                 }
                    #             ]
                    #         }
                    #     ],
                    #     "booked_hours": [
                    #         {
                    #             "unit": 8,
                    #             "unit_of_measure": "Uur",
                    #             "start_datetime": "2023-02-01T09:00:00",
                    #             "end_datetime": "2023-02-01T17:00:00"
                    #         }
                    #     ]
                    # }
                ]
            }
        ]
    }
]

td_template_2 = [
    {
        "Description": "Testcase template 2 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL + 2024)",
        "Amount": 10, # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 2 weken verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-03-14"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    },
                    # {
                    #     "function": [
                    #         {
                    #             "label": "AOK Wel ZVL ",
                    #             "caregiving_role": True,
                    #             "start_date": "2024-01-01",
                    #             "end_date": "2024-12-31",
                    #         }
                    #     ],
                    #     "start_date": "2024-01-01",
                    #     "end_date": "2024-12-31",                        
                    #     "contract_agreement": [
                    #         {
                    #             "start_date": "2024-01-01",
                    #             "end_date": "2024-12-31",
                    #             "location": "Vestiging_Grotestraat",                       
                    #             "size": [
                    #                 {
                    #                     "unit": 1,
                    #                     "unit_of_measure": "fte_36",
                    #                     "start_date": "2024-01-01",
                    #                     "end_date": "2024-12-31"
                    #                 }
                    #             ],
                    #         }
                    #     ],
                    #     "booked_hours": [
                    #         {
                    #             "unit": 8,
                    #             "unit_of_measure": "Uur",
                    #             "start_datetime": "2024-02-01T09:00:00",
                    #             "end_datetime": "2024-02-01T17:00:00"
                    #         }
                    #     ]
                    # }
                ]
            }
        ]
    }
]



td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL Geen verlof",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wel ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL Geen verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]                


td_03 = [ 
    {
        "Description": "Testcase 03 (Geen ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL Geen verzuim/verlof",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Geen verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL Geen verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]


td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 2 weken verzuim/verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-03-14"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]


td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Teller: 14 * 0.5 (ptf) * 10 = 70
                      # Noemer: 
                      # Indicator score: 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (18u p/w) 2 weken verzuim",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 25,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-03-14"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_05_c = [
    {
        "Description": "Testcase 05c (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (18u p/w) 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_05_d = [
    {
        "Description": "Testcase 05d (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Teller: 14 * 10 (aantal humans) = 140
                      # Noemer: 6628.666...
                      # Indicator: 2.112...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_05_e = [
    {
        "Description": "Testcase 05e (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Teller: 14 * 0.5 (ptf) * 10 (aantal humans) = 70
                      # Noemer: 4798.666...
                      # Indicator: 1.458...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (18u p/w) 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 25,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_05_f = [
    {
        "Description": "Testcase 05f (Wel ZVL functie + Wel AOK + Geen parttimefactor + 6 weken verzuim (3x2 weken) + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)",
        "Amount": 10, # Teller: 14 * 10 (aantal humans) = 140
                      # Noemer: 6628.666...
                      # Indicator: 2.112...
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            },
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-14"
                            },
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-07-01",
                                "end_date": "2024-07-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL (18u p/w) 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 25,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]                

td_06_a = [
    {
        "Description": "Testcase 06a (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]                


td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL (18u p/w) 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 25,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Geen ZVL functie + Geen AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 25,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL 2 weken verzuim",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 25,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]


td_08_b = [
    {
        "Description": "Testcase 08b (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 0.5,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]

td_08_c = [
    {
        "Description": "Testcase 08c (Geen ZVL functie + Geen AOK + Geen parttimefactor + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL 2 weken verzuim, 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 75,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]


td_08_d = [
    {
        "Description": "Testcase 08d (Geen ZVL functie + Geen AOK + Geen parttimefactor + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL 3 maanden verlof",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "maternity_leave": [
                            {
                                "unit": 100,
                                "unit_of_measure": "percent",
                                "start_date": "2024-03-01",
                                "end_date": "2024-05-30"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]                

td_08_e = [
    {
        "Description": "Testcase 08e (Geen ZVL functie + Geen AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL 2 weken verzuim",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                       
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ],
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 50,
                                "unit_of_measure": "percent",
                                "start_date": "2024-02-01",
                                "end_date": "2024-02-14"
                            }
                        ],
                        # "booked_hours": [
                        #     {
                        #         "unit": 8,
                        #         "unit_of_measure": "Uur",
                        #         "start_datetime": "2024-01-01T09:00:00",
                        #         "end_datetime": "2024-01-01T17:00:00"
                        #     }
                        # ]
                    }
                ]
            }
        ]
    }
]


# Static Tests
def test_if_headers_are_correct_for_query_1_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """
    dg = DataGenerator(db_config, td_template_1)

    try:

        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')
        
        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")
        
        # Assertions
        test.verify_header_present("teller")
        test.verify_header_present("noemer")
        test.verify_header_present("indicator")
    finally:
        dg.delete_graph_data()


def test_if_number_of_rows_returned_is_correct_for_query_1_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')
        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")
        # Assertions
        test.verify_row_count(1)
    finally:
        dg.delete_graph_data()


def test_if_indicator_has_correct_value_for_query_1_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')
        
        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")
        
        # Assertions
        test.verify_value_in_list("indicator", {"1.917808219178082191780822","1.9178082191780821917808220"})
        # .in_row(1)
    finally:
        dg.delete_graph_data()


def test_if_dates_can_change_1_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_2)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Assertions
        test.verify_value_in_list("indicator", {"1.912568306010928961748634","1.9125683060109289617486340"})
    finally:
        dg.delete_graph_data()


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_1_1_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_1_1_02(db_config):
    """ Testcase 02 (Wel ZVL functie + Geen AOK + Geen parttimefactor (Inzet: 36u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_1_1_03(db_config):
    """ Testcase 03 (Geen ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_1_1_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK + Geen parttimefactor + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04a
def test_if_value_returned_is_correct_for_query_1_1_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 05
def test_if_value_returned_is_correct_for_query_1_1_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"1.912568306010928961748634", "1.9125683060109289617486340"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 05a
def test_if_value_returned_is_correct_for_query_1_1_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0.956284153005464480874317", "0.956284153005464480874317"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_1_1_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie + Wel AOK (12m) + Geen parttimefactor + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c
def test_if_value_returned_is_correct_for_query_1_1_05_c(db_config):
    """ Testcase 05c (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05d
def test_if_value_returned_is_correct_for_query_1_1_05_d(db_config):
    """ Testcase 05d (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"1.912568306010928961748634", "1.9125683060109289617486340"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05e
def test_if_value_returned_is_correct_for_query_1_1_05_e(db_config):
    """ Testcase 05e (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0.956284153005464480874317", "0.9562841530054644808743170"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05f
def test_if_value_returned_is_correct_for_query_1_1_05_f(db_config):
    """ Testcase 05f (Wel ZVL functie + Wel AOK + Geen parttimefactor + 6 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Wel PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"9.562841530054644808743169", "9.5628415300546448087431690"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_1_1_06(db_config):
    """ Testcase 06 (Wel ZVL functie + Wel AOK + Wel parttimefactor (inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 06a
def test_if_value_returned_is_correct_for_query_1_1_06_a(db_config):
    """ Testcase 06a (Wel ZVL functie + Wel AOK + Geen parttimefactor + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 07
def test_if_value_returned_is_correct_for_query_1_1_07(db_config):
    """ Testcase 07 (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 07a
def test_if_value_returned_is_correct_for_query_1_1_07_a(db_config):
    """ Testcase 07a (Geen ZVL functie + Geen AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_1_1_08(db_config):
    """ Testcase 08 (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 08a
def test_if_value_returned_is_correct_for_query_1_1_08_a(db_config):
    """ Testcase 08a (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_1_1_08_b(db_config):
    """ Testcase 08b (Geen ZVL functie + Geen AOK + Wel parttimefactor (Inzet 18u p/w) + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 08c
def test_if_value_returned_is_correct_for_query_1_1_08_c(db_config):
    """ Testcase 08c (Geen ZVL functie + Geen AOK + Geen parttimefactor + 2 weken verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08d
def test_if_value_returned_is_correct_for_query_1_1_08_d(db_config):
    """ Testcase 08d (Geen ZVL functie + Geen AOK + Geen parttimefactor + Geen verzuim + 3 maanden Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 08e
def test_if_value_returned_is_correct_for_query_1_1_08_e(db_config):
    """ Testcase 08e (Geen ZVL functie + Geen AOK + Geen parttimefactor + 2 weken verzuim + Geen Zwangerschaps-bevallingsverlof + Geen PIL)
        NZA Basisinformatie kostenonderzoek 1.1. Ziekteverzuimpercentage
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 1.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"Ongedefineerd","Ongedefineerd"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()