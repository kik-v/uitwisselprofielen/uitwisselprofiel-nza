from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:
td_template_1 = [
    {
        "Description": "Testcase template 1 (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + 2023) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase template 1 (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + 2023) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422400" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422410" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422600" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422900" ,
                "bedrag": 10000  
            }           
        ] 
    }
]

td_template_2 = [
    {
        "Description": "Testcase template 2 (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + 2024) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase template 2 (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + 2024) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900" ,
                "bedrag": 10000  
            }           
        ] 
    }
]

td_01 = [
    {
        "Description": "Testcase 01 (Client + Geen matching rubrieken PIL (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ],    
    },
    {
        "Description": "Testcase 01 (Client + Geen matching rubrieken PIL (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1, 
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "bedrag": 10000  
            }
        ], 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Client + Geen matching rubrieken PIL (ZZP kosten) + Valt wel in meetperiode)",
        "Amount": 10, 
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Vestiging_De_Beuk",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], # "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": ["pgb"]
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 02 (Client + Geen matching rubrieken PIL (ZZP kosten) + Valt wel in meetperiode)",
        "Amount": 1, 
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "bedrag": 10000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (INHOK + Wel matching rubrieken PIL (Sociale lasten) + Valt niet in meetperiode)",
        "Amount": 10, #Indicator score: nvt
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH. OK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 03 (INHOK + Wel matching rubrieken PIL (Sociale lasten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "WPerSol" ,
                "bedrag": 10000  
            },
        ] 
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (AOK + Geen matching rubrieken PIL (ZZP) + Valt niet in meetperiode) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 04 (AOK + Geen matching rubrieken PIL (ZZP) + Valt niet in meetperiode) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "bedrag": 10000  
            }
        ] 
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (INHOK + Wel matching rubrieken PIL (Sociale lasten) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score:  nvt
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 05 (INHOK + Wel matching rubrieken PIL (Sociale lasten) + Valt wel in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WBedOvpPerLes",
                "bedrag": 10000  
            }
        ] 
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (AOK + Wel matching rubrieken PIL (Sociale lasten) + Valt niet in meetperiode) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 06 (AOK + Wel matching rubrieken PIL (Sociale lasten) + Valt niet in meetperiode) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpPerLes",
                "bedrag": 10000  
            }
        ] 
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (AOK + Geen matching rubrieken PIL (ZZP) + Valt wel in meetperiode) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 07 (AOK + Geen matching rubrieken PIL (ZZP) + Valt wel in meetperiode) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "bedrag": 10000  
            }
        ] 
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (AOK + Wel matching rubrieken PIL (PerLes, PerSol & Prismant 411 t/m 413) + Valt wel in meetperiode) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 08 (AOK + Wel matching rubrieken PIL (PerLes, PerSol & Prismant 411 t/m 413) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WPerSol",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WPerLes",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411200" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411400" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411600" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411700" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412200" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412400" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413200" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413400" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413600" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413610" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413620" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413700" ,
                "bedrag": 10000  
            }
        ] 
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (AOK + Wel matching rubrieken PIL (Prismant 414 t/m 415) + Valt wel in meetperiode) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 08a (AOK + Wel matching rubrieken PIL (Prismant 414 t/m 415) + Valt wel in meetperiode) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414101" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414102" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414103" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414104" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414110" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414111" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414112" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414113" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414114" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414200" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414202" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414203" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414204" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414503" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414504" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414700" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414800" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "415000" ,
                "bedrag": 10000  
            }            
        ] 
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + Valt wel in meetperiode) ",
        "Amount": 10, #Indicator score:  nvt
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 1,
                                        "unit_of_measure": "fte_36",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31",
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {
        "Description": "Testcase 08b (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + Valt wel in meetperiode) ",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900" ,
                "bedrag": 10000  
            }           
        ] 
    }
]

# Static Tests
def test_if_headers_are_correct_for_query_3_2(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        NZA Basisinformatie kostenonderzoek 3.2. Gemiddeld bruto jaarsalaris per fte
    """
    dg = DataGenerator(db_config, td_template_1)

    try:

        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')
        
        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")
        
        # Assertions
        test.verify_header_present("teller")
        test.verify_header_present("noemer")
        test.verify_header_present("indicator")
    finally:
        dg.delete_graph_data()


def test_if_number_of_rows_returned_is_correct_for_query_3_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        NZA Basisinformatie kostenonderzoek 3.2. Gemiddeld bruto jaarsalaris per fte
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')
        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")
        # Assertions
        test.verify_row_count(1)
    finally:
        dg.delete_graph_data()


def test_if_indicator_has_correct_value_for_query_3_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        NZA Basisinformatie kostenonderzoek 3.2. Gemiddeld bruto jaarsalaris per fte
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')
        
        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")
        
        # Assertions
        test.verify_value_in_list("indicator", {"7000","7000.0"})
        # .in_row(1)
    finally:
        dg.delete_graph_data()


def test_if_dates_can_change_3_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        NZA Basisinformatie kostenonderzoek 3.2. Gemiddeld bruto jaarsalaris per fte
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_2)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Assertions
        test.verify_value_in_list("indicator", {"7000","7000.0"})
    finally:
        dg.delete_graph_data()


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_3_2_01(db_config):
    """ Testcase 01 (Client + Geen matching rubrieken PIL (ZZP kosten) + Valt niet in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"9.222222222222222222222222","9.222222222222222222222222"})           
        test.verify_value_in_list("indicator", {None,None})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_3_2_02(db_config):
    """ Testcase 02 (Client + Geen matching rubrieken PIL (ZZP kosten) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"9.222222222222222222222222","9.222222222222222222222222"})           
        test.verify_value_in_list("indicator", {None, None})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_3_2_03(db_config):
    """ Testcase 03 (INHOK + Wel matching rubrieken PIL (Sociale lasten) + Valt niet in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"9.222222222222222222222222","9.222222222222222222222222"})        
        test.verify_value_in_list("indicator", {None,None})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_3_2_04(db_config):
    """ Testcase 04 (AOK + Geen matching rubrieken PIL (ZZP) + Valt niet in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"10.222222222222222222222222","10.222222222222222222222222"})        
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_3_2_05(db_config):
    """ Testcase 05 (INHOK + Wel matching rubrieken PIL (Sociale lasten) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"9.222222222222222222222222","9.222222222222222222222222"})
        test.verify_value_in_list("indicator", {None,None})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_3_2_06(db_config):
    """ Testcase 06 (AOK + Wel matching rubrieken PIL (Sociale lasten) + Valt niet in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"10.222222222222222222222222","10.222222222222222222222222"})
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_3_2_07(db_config):
    """ Testcase 07 (AOK + Geen matching rubrieken PIL (ZZP) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {None,None})
        # test.verify_value_in_list("noemer", {"10.222222222222222222222222","10.222222222222222222222222"})
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_3_2_08(db_config):
    """ Testcase 08 (AOK + Wel matching rubrieken PIL (PerLes, PerSol & Prismant 411 t/m 413) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {"260000","260000.0"})
        # test.verify_value_in_list("noemer", {"10","10.0"})
        test.verify_value_in_list("indicator", {"26000","26000.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a
def test_if_value_returned_is_correct_for_query_3_2_08_a(db_config):
    """ Testcase 08a (AOK + Wel matching rubrieken PIL (Prismant 414 t/m 415) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {"220000","220000.0"})
        # test.verify_value_in_list("noemer", {"10","10.0"})
        test.verify_value_in_list("indicator", {"22000","22000.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_3_2_08_b(db_config):
    """ Testcase 08b (AOK + Wel matching rubrieken PIL (Prismant 4221 t/m 4229) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 3.2. Wat is het Gemiddeld bruto jaarsalaris per fte?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 3.2.rq')

        # Change measuring period parameters of query
        # test.set_start_period_new_param_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        # test.verify_value_in_list("teller", {"70000","70000.0"})
        # test.verify_value_in_list("noemer", {"10", "10.0"})
        test.verify_value_in_list("indicator", {"7000","7000.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()