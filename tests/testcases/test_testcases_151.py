from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: "2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:

td_template_1 = [
    {
        "Description": "Testcase template 1 (Afschrijvingskosten, Totaal Activa (000011 t/m 311100), Passiva (051100 t/m 151300), Rentelasten (900000 - 904000), Bedrijfsopbrengsten (811000 t/m 930000) & Bedrijfslasten (417000 - 931000)  2023-01-01) + Valt wel in meetperiode)",
        "Amount": 10, #Indicator score:  209 declaraties van 10000 each. 2090000 totaal (Activa & passiva), Bedrsijfsopbrengsten: 16 declarations of 10000 each = 160000, Bedrijfslasten & Rentelasten: 131 + 6 declarations of 10000 each = 1370000
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {        
        "Description": "Testcase template 1 (Vervoersmiddelen & immateriele vaste activa, PIL Rubrieken, Totaal Activa (000011 t/m 311100), Passiva (051100 t/m 151300), Rentelasten (900000 - 904000), Bedrijfsopbrengsten (811000 t/m 930000) & Bedrijfslasten (417000 - 931000)  2023-01-01) + Valt wel in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            # Huur installaties
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484232" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486725" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486735" ,
                "bedrag": 10000  
            }, 
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486515" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486525" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486535" ,
                "bedrag": 10000  
            },
            # Huur medische inventaris, inventaris, vervoersmiddelen & automatisering
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486615" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486625" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486635" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486825" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486835" ,
                "bedrag": 10000  
            },
            # Dotaties
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "489150" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "489211" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "489212" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "489221" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "489222" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "489230" ,
                "bedrag": 10000  
            },
            # Huur terreinvoorzieningen
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486115" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486125" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486135" ,
                "bedrag": 10000  
            },
            # Huur gebouwen
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486215" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486225" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486235" ,
                "bedrag": 10000  
            },
            # Huur semi permanente gebouwen
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486315" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486325" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486335" ,
                "bedrag": 10000  
            },
            # Huur houten paviljoens
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486415" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486425" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "486435" ,
                "bedrag": 10000  
            },
        # Afschrijvingskosten interest
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485190" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485205" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485211" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485311" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485412" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485900" ,
                "bedrag": 10000  
            },
        # PIL rubrieken
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WPerSol",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WPerLes",
                "bedrag": 10000  
            },
        # Totaal activa: 000011 - 311100
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000011",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000012",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000021",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000022",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000031",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000032",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000211",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000311",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000312",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000411",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000412",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000531",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000532",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000631",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000632",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000731",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000732",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000831",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000832",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000931",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "000932",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011130",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011211",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011221",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011231",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011241",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011251",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "011252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012141",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012151",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012211",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012221",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012231",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012241",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012251",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012311",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012312",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012321",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012331",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012341",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012342",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012351",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012352",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012411",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012412",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012421",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012422",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012431",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012432",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012441",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012442",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012451",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "012452",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013141",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013151",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "013152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014221",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014231",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014321",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014331",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "014332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "015110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "015120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "015130",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "015140",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "015150",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031130",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031220",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031230",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "031401",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "032101",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "032210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "032510",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "032610",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "032910",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "311100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "311200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "311300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "311400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "311500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "311600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "312000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "319000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "121000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "121001",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "121002",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "121003",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "123100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "123200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "123300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "124111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "124112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "124113",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "125000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "126100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "126110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "128100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "128200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "128310",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "128320",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "128330",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "131100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "131200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "131900",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "132100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "132200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "132210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "132400",
                "bedrag": 10000
            },
        # Totaal passiva: 051100 - 151300    
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "051100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "054130",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "053110",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "053420",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "054230",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "053230",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "053900",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "054930",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "059150",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "091000",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "061000",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "061950",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "061960",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "151100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "071100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "071200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "071210",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "071220",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "072100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "072200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "072300",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "072900",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "073100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "073200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "141010",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "141020",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "141110",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "141120",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "151400",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152210",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152220",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152310",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152320",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152410",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152420",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152610",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152620",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152710",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152720",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "152900",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "153100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "153200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "153300",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "154100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "154200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "155000",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "158100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "158200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "158310",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "158320",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "158330",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "151200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "151300",
                "bedrag": 10000  
            },
                    # Bedrijfsopbrengsten: 811000 t/m 930000
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "811000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "821000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "822000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "825000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "826000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "827000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "828000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "829000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "831000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "832000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "833000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "835000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "891000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "919000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "920000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "930000",
                "bedrag": 10000
            },
        # Bedrijfslasten: 417000 - 931000
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "417000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "418000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "418100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "418200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "411700",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "412000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "412100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "412200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "412300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "412400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "412500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413610",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413620",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "413700",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414101",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414102",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414103",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414104",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414113",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414114",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414202",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414203",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414204",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414503",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414504",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414700",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "414800",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "415000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "416000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "419000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "419100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "419101",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "419200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "419500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "420000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "420100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422410",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422900",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "422600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480012",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480022",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480032",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480532",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480632",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "480932",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "481212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "481222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "481232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "481242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "481252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482312",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482342",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482352",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482412",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482422",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482432",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482442",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "482452",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "483112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "483122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "483132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "483142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "483152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "484332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423140",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423190",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423220",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423410",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "423420",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "905000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "911000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "912000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "913000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "914000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "915000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "921000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "931000",
                "bedrag": 10000
            },
        # Rentelasten: 900000 - 904000 & 485000
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "904000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "900000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "903000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "485000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "901000",
                "bedrag": 10000
            },
        # Resultaat deelnemingen: 902000
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "902000",
                "bedrag": 10000
            },
        ]
    }
]


td_template_2 = [
    {
        "Description": "Testcase template 2 (Afschrijvingskosten, Totaal Activa (000011 t/m 311100), Passiva (051100 t/m 151300), Rentelasten (900000 - 904000), Bedrijfsopbrengsten (811000 t/m 930000) & Bedrijfslasten (417000 - 931000)  2024-01-01) + Valt wel in meetperiode)",
        "Amount": 10, #Indicator score:  209 declaraties van 10000 each. 2090000 totaal (Activa & passiva), Bedrsijfsopbrengsten: 16 declarations of 10000 each = 160000, Bedrijfslasten & Rentelasten: 131 + 6 declarations of 10000 each = 1370000
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31", 
                        "contract_agreement": [
                            {
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "location": "Vestiging_Grotestraat",                         
                                "size": [
                                    {
                                        "unit": 36,
                                        "unit_of_measure": "Uren_per_week_unit",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                    }
                ]
            }
        ],
    },
    {        
        "Description": "Testcase template 2 (Vervoersmiddelen & immateriele vaste activa, PIL Rubrieken, Totaal Activa (000011 t/m 311100), Passiva (051100 t/m 151300), Rentelasten (900000 - 904000), Bedrijfsopbrengsten (811000 t/m 930000) & Bedrijfslasten (417000 - 931000)  2024-01-01) + Valt wel in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            # Huur installaties
                        {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484232" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486725" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486735" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486515" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486525" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486535" ,
                "bedrag": 10000  
            },
            # Huur medische inventaris, inventaris, vervoersmiddelen & automatisering
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486615" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486625" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486635" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486825" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486835" ,
                "bedrag": 10000  
            },
            # Dotaties
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489150" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489211" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489212" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489221" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489222" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "489230" ,
                "bedrag": 10000  
            },
            # Huur terreinvoorzieningen
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486115" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486125" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486135" ,
                "bedrag": 10000  
            },
            # Huur gebouwen
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486215" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486225" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486235" ,
                "bedrag": 10000  
            },
            # Huur semi permanente gebouwen
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486315" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486325" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486335" ,
                "bedrag": 10000  
            },
            # Huur houten paviljoens
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486415" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486425" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "486435" ,
                "bedrag": 10000  
            },
        # Afschrijvingskosten interest
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485100" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485190" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485205" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485211" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485300" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485311" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485412" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485900" ,
                "bedrag": 10000  
            },
        # PIL rubrieken
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WPerSol",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type":  "WPerLes",
                "bedrag": 10000  
            },
        # Totaal activa: 000011 - 311100
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000011",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000012",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000021",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000022",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000031",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000032",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000211",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000311",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000312",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000411",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000412",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000531",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000532",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000631",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000632",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000731",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000732",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000831",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000832",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000931",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "000932",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011130",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011211",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011221",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011231",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011241",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011251",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "011252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012141",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012151",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012211",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012221",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012231",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012241",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012251",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012311",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012312",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012321",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012331",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012341",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012342",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012351",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012352",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012411",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012412",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012421",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012422",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012431",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012432",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012441",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012442",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012451",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "012452",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013141",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013151",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "013152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014121",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014131",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014221",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014231",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014321",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014331",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "014332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "015110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "015120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "015130",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "015140",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "015150",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031130",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031220",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031230",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "031401",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "032101",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "032210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "032510",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "032610",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "032910",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "311600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "312000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "319000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121001",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121002",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "121003",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "123300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "124113",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "125000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "126110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128310",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128320",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "128330",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "131900",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "132400",
                "bedrag": 10000
            },
        # Totaal passiva: 051100 - 151300    
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "051100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "054130",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053110",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053420",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "054230",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053230",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "053900",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "054930",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "059150",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "091000",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "061000",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "061950",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "061960",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "071100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "071200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "071210",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "071220",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "072100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "072200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "072300",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "072900",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "073100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "073200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141010",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141020",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141110",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "141120",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151400",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152210",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152220",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152310",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152320",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152410",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152420",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152610",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152620",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152710",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152720",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "152900",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "153300",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "154100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "154200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "155000",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158100",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158310",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158320",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "158330",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151200",
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "151300",
                "bedrag": 10000  
            },
                    # Bedrijfsopbrengsten: 811000 t/m 930000
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "891000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "919000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "920000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "930000",
                "bedrag": 10000
            },
        # Bedrijfslasten: 417000 - 931000
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "417000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "418200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "411700",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "412500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413610",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413620",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "413700",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414101",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414102",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414103",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414104",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414111",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414113",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414114",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414202",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414203",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414204",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414503",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414504",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414700",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "414800",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "415000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "416000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419101",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "419500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "420100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422410",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422500",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422900",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "422600",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480012",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480022",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480032",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480532",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480632",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "480932",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "481252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482212",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482242",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482252",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482312",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482342",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482352",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482412",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482422",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482432",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482442",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "482452",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483142",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "483152",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484112",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484122",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484132",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484222",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484232",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484322",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "484332",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423100",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423110",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423120",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423140",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423190",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423200",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423210",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423220",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423300",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423400",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423410",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "423420",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "905000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "911000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "912000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "913000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "914000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "915000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "921000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "931000",
                "bedrag": 10000
            },
        # Rentelasten: 900000 - 904000 & 485000
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "904000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "900000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "903000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "485000",
                "bedrag": 10000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "901000",
                "bedrag": 10000
            },
        # Resultaat deelnemingen: 902000
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "902000",
                "bedrag": 10000
            },
        ]
    }
]


td_01 = [
    {
        "Description": "Testcase 01 (Geen matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (ZZP kosten) + Valt niet in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type":  "WBedOvpZzp",
                "bedrag": 10000  
            }
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (Sociale kosten) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerSol" ,
                "bedrag": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (800000) + Valt niet in meetperiode)",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "800000" ,
                "bedrag": 10000  
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (Rubrieken 811000 t/m 835000) + Valt wel in meetperiode)",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [                      
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "811000" ,
                "bedrag": 10000  
            },           
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "821000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "822000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "825000" ,
                "bedrag": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "826000" ,
                "bedrag": 10000  
            },       
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "827000" ,
                "bedrag": 10000  
            },  
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "828000" ,
                "bedrag": 10000  
            },      
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "829000" ,
                "bedrag": 10000  
            },     
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "831000" ,
                "bedrag": 10000  
            },    
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "832000" ,
                "bedrag": 10000  
            },      
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "833000" ,
                "bedrag": 10000  
            },     
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "835000" ,
                "bedrag": 10000  
            },                   
        ] 
    }
]

# Static Tests
def test_if_headers_are_correct_for_query_15_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")

        # Assertions
        test.verify_header_present('totaal')
    finally:
        dg.delete_graph_data()

def test_if_number_of_rows_returned_is_correct_for_query_15_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)? 
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")

        # Assertions
        test.verify_row_count(1)
    finally:
        dg.delete_graph_data()

def test_if_indicator_has_correct_value_for_query_15_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_1)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        test.change_start_period("2024-01-01","2023-01-01")
        test.change_end_period("2024-12-31","2023-12-31")

        # Assertions
        test.verify_value_in_list("totaal", {"120000","120000.0"})
    finally:
        dg.delete_graph_data()
    

def test_if_dates_can_change_15_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """
    # Load defined test data
    dg = DataGenerator(db_config, td_template_2)

    try:
        # Setup of the test
        test = QueryTest(db_config)

        # Configuration and execution
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Assertions
        test.verify_value_in_list("totaal", {"120000","120000.0"})
    finally:
        dg.delete_graph_data()

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_15_1_01(db_config):
    """ Testcase 01 (Geen matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (ZZP kosten) + Valt niet in meetperiode)
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("totaal", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_15_1_02(db_config):
    """ Testcase 02 (Geen matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (Sociale kosten) + Valt wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("totaal", {"0", "0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_15_1_03(db_config):
    """ Testcase 03 (Wel matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (800000) + Valt niet in meetperiode)
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("totaal", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_15_1_04(db_config):
    """ Testcase 04 (Wel matching rubrieken totale opbrengsten (na correctie exploitatieresultaten) (Rubrieken 811000 t/m 835000) + Valt Wel in meetperiode)
        NZA Basisinformatie kostenonderzoek 15.1. Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/Indicator 15.1.rq')

        # Change measuring period parameters of query
        # test.set_start_period_to("2024-01-01")
        # test.set_end_period_to("2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("totaal", {"120000","120000.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()