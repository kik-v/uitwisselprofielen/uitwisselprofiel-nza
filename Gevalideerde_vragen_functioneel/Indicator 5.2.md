---
title: 5.2. Wat zijn andere hotelmatige kosten?
description: "Het totaal aan materiële kosten die aan de hotelfunctie van de instelling kunnen worden toegerekend."
weight: 5
---
## Indicator

**Definitie:** Het totaal aan materiële kosten die aan de hotelfunctie van de instelling kunnen worden toegerekend.

**Teller:** Totaal aan andere hotelmatige kosten.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan kosten die aan de hotelfunctie van de instelling kunnen worden toegerekend, met uitzondering van de kosten van vervoer over de periode (Bron: Gebruikersinstructie Kostenonderzoek 2016 - Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

De indicator wordt op organisatieniveau berekend.

Voor de berekening van deze indicator hanteert de NZa het Prismant rekeningsschema (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 35).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Niet geïmplementeerd vanuit RGS |

| Specificatie rubrieken Prismant|
|----------------|
| 441000 Huishouding en inrichting |
| 441100 Schoonmaakkosten |
| 441200 Afvalverwijdering en -verwerking |
| 441300 Toiletbenodigdheden |
| 441400 Meubilair en stoffering |
| 441900 Automaten, kosten van huishouding en inrichting |
| 442000 Linnenvoorziening |
| 442100 Textiel |
| 442200 Waskosten |
| 444000 Huishoudgeld voor afdelingen |
| 444100 Uitgekeerd huishoudgeld voor bewoners |
| 445000 Kosten beveiliging en bewaking |
| 445100 Kosten beveiliging en bewaking door derden |
| 445200 Kosten eigen beveiliging en bewaking |
| 449000 Andere hotelmatige voorzieningen |

## Uitgangspunten

* Alle andere hotelmatige kosten worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van andere hotelmatige kosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal andere hotelmatige kosten (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
