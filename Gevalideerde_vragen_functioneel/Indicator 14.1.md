---
title: 14.1 Wat zijn de correctie budgetafrekeningen voorgaande boekjaren? 
description: "Correctie budgetafrekeningen over voorgaande jaren kunnen naderhand met de NZa overeengekomen aanpassingen van het wettelijk budget voor aanvaardbare kosten worden verantwoord."
weight: 14
---
## Indicator

**Definitie:** Correctie budgetafrekeningen over voorgaande jaren kunnen naderhand met de NZa overeengekomen aanpassingen van het wettelijk budget voor aanvaardbare kosten worden verantwoord.  

**Teller:** Correctie budgetafrekeningen voorgaande boekjaren

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft de correctie budgetafrekeningen voorgaande boekjaren. De indicator wordt op organisatieniveau berekend.

Voor de correctie budgetafrekeningen voorgaande boekjaren hanteert de NZa de definitie volgens het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 16).

Correctie budgetafrekeningen over voorgaande jaren kunnen naderhand met de NZa overeengekomen aanpassingen van het wettelijk budget voor aanvaardbare kosten worden verantwoord.

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| 891000 Correctie budgetafrekeningen voorgaande boekjaren |

## Uitgangspunten

* Geen

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van de correctie budgetafrekeningen voorgaande boekjaren voor de betreffende meetperiode bij elkaar op.

| Organisatieonderdeel       | Totaal correctie budgetafrekeningen voorgaande boekjaren (euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
