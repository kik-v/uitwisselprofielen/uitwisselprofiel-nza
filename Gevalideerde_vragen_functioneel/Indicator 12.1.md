---
title: 12.1. Wat is het wettelijk toegekend budget?
description: "Hier wordt het toegekende budget volgens de laatste rekenstaat van de NZa verantwoord. Deze budgetten worden uitgesplitst opgevraagd: Wlz, niet Wlz en Wlz PGB en ELV. De productcodes van de producten die behoren tot de Wlz en ELV zijn opgenomen in het tabblad producten."
weight: 12
---
## Indicator

**Definitie:** Hier wordt het toegekende budget volgens de laatste rekenstaat van de NZa verantwoord. Deze budgetten worden uitgesplitst opgevraagd: Wlz, niet Wlz en Wlz PGB en ELV. De productcodes van de producten die behoren tot de Wlz en ELV zijn opgenomen in het tabblad producten.

**Teller:** Het wettelijk toegekend budget

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het toegekend wettelijk budget. De indicator wordt op organisatieniveau berekend.

Het toegekend wettelijk budget bestaat uit aanvaardbare kosten die de zorgaanbieder ontvangt. Het is het budget dat de zorgverzekeraar betaalt voor de te leveren zorg. In het Prismant rekeningsschema zijn dit alle rekeningen uit rubriek 81. Dit gaan over het toegekend en berekend wettelijk budget In deze rubriek zijn opgenomen de rekeningen voor zorginstellingen voor opbrengsten die voortvloeien uit het met financiers overeengekomen wettelijk budget.

Voor deze indicator geldt specifiek de rekening 811000 ‘Toegekend wettelijk budget’ (bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG- versie 12 september 2017, pagina 16).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| 811000 Toegekend wettelijk budget  |

## Uitgangspunten

* Prismant: Het toegekend Wettelijk budget betreft tevens de "Berekende correcties op het toegekend wettelijk budget" zoals beschreven in Prismant pagina 44.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van het wettelijk toegekende budget voor de betreffende meetperiode bij elkaar op.

| Organisatieonderdeel       | Totaal Toegekend wettelijk budget (euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
