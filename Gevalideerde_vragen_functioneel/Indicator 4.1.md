---
title: 4.1. Sociale kosten
description: "De kosten die voortvloeien uit wettelijke dan wel sociale verplichtingen van de werkgever, die niet leiden tot rechtstreekse uitbetalingen door de werkgever aan het personeel, zoals betaalde (verzekerings-)premies in verband met ziekte, werkloosheid, arbeidsongeschiktheid en pensioen."
weight: 4
---
## Indicator

**Definitie:** De kosten die voortvloeien uit wettelijke dan wel sociale verplichtingen van de werkgever, die niet leiden tot rechtstreekse uitbetalingen door de werkgever aan het personeel, zoals betaalde (verzekerings-)premies in verband met ziekte, werkloosheid, arbeidsongeschiktheid en pensioen. Met betrekking tot personeel in loondienst.

**Teller:** Alle sociale kosten.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft de sociale kosten van personeelsleden in loondienst over de periode. De indicator wordt op organisatieniveau berekend.

Sociale kosten zijn de kosten die voortvloeien uit wettelijke dan wel sociale verplichtingen van de werkgever, die niet leiden tot rechtstreekse
uitbetalingen door de werkgever aan het personeel, zoals betaalde (verzekerings-)premies in verband met ziekte, werkloosheid, arbeidsongeschiktheid en pensioen. In het Prismant rekeningsschema zijn dit alle rekeningen uit rubriek 42 (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
|   WPerSol        S.B    4002000    Sociale lasten    Sociale lasten |
|   WPerSolPsv        S.B.A    4002010    Premies sociale verzekeringen    Premies sociale verzekeringen sociale lasten|
|   WPerSolBiz        S.B.C    4002020    Bijdrage ziektekostenverzekering    Bijdrage ziektekostenverzekering sociale lasten|
|   WPerSolOpr        S.B.D    4002030    Overige premies    Overige premies sociale lasten|
|   WPerSolOsf        S.B.E    4002040    Overige sociale fondsen    Overige sociale fondsen sociale lasten|
|   WPerSolOss        S.B.F    4002050    Overige sociale lasten    Overige sociale lasten sociale lasten|

| Specificatie rubrieken Prismant|
|----------------|
|422100 Berekende sociale kosten vakantiebijslag |
|422300 Aandeel werknemer premies sociale verzekeringen|
|422400 Aandeel werkgever premies sociale verzekeringen |
|422410 Korting / vrijlating basispremie WAO / werkgeversdeel Awf (Algemeen werkloosheidsfonds)|
|422500 Ziektekostenverzekering |
|422600 Pensioenkosten |
|422900 Overgeboekte sociale kosten |

## Uitgangspunten

* Alle sociale kosten van personeelsleden in loondienst worden geïncludeerd.
* Prismant: Sociale kosten voor eindejaarsuitkeringen worden gezien als "422100 Berekende sociale kosten vakantiebijslag".

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van sociale kosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal sociale kosten PIL (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
