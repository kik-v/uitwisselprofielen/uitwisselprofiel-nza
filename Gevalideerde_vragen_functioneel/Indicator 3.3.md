---
title: 3.3. Loonkosten voor personeel in loondienst (PIL)
description: "Totaal aan loonkosten voor personeel in loondienst (PIL)."
weight: 3
---
## Indicator

**Definitie:** Totaal aan loonkosten voor personeel in loondienst (PIL).

**Teller:** Alle loonkosten van personeel in loondienst.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft de totale loonkosten van personeel in loondienst (PIL) over de periode. De indicator wordt op organisatieniveau berekend.

Voor loonkosten van personeel in loondienst (PIL) hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Loonkosten van personeel in loondienst betreft de salariskosten van in dienstbetrekking verrichte arbeid op basis van een arbeidsovereenkomst. De instelling treedt op als werkgever en is uit dien hoofde inhoudingsplichtig voor de loonbelasting en premieheffing. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 29).

Op grond van hiervan worden tot de salarissen gerekend:

* Brutosalaris (volgens inschalingtabellen) inclusief doorbetaalde salarissen tijdens ziekte;
* Vakantiebijslag;
* Vergoedingen voor overwerk, onregelmatige dienst, bereikbaarheids-, aanwezigheids- en slaapdienst;
* Eindejaarsuitkeringen e.d. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 30).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WPerLes S.A 4001000 Lonen en salarissen |
| WPerSol S.B 4002000 Sociale lasten |

| Specificatie rubrieken Prismant|
|----------------|
| 411 Algemene en administratieve functies |
| 412 Hotelfuncties |
| 413 Patiënt- c.q. bewonergebonden functies |
| 414 Leerling personeel |
| 415 Terrein- en gebouwgebonden functies |
| 422100 Berekende sociale kosten vakantiebijslag |
| 422300 Aandeel werknemer premies sociale verzekeringen |
| 422400 Aandeel werkgever premies sociale verzekeringen |
| 422410 Korting / vrijlating basispremie WAO / werkgeversdeel Awf (Algemeen werkloosheidsfonds) |
| 422500 Ziektekostenverzekering |
| 422600 Pensioenkosten |
| 422900 Overgeboekte sociale kosten |

## Uitgangspunten

* Alle loonkosten van alle personeelsleden in loondienst (PIL) worden geïncludeerd.
* Bezoldiging bestuurders en comissarissen wel (RGS) of juist niet (Prismant) meenemen bij deze indicator (deze is nu meegenomen bij deze indicator)?

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van loonkosten van personeel in loondienst (PIL) voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal directe loonkosten PIL (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
