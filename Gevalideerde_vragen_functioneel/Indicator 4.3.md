---
title: 4.3. Overige personeelskosten van personeel in loondienst (PIL)
description: "Totaal aan overige personeelskosten van personeel in loondienst (PIL)."
weight: 4
---
## Indicator

**Definitie:** Totaal aan overige personeelskosten van personeel in loondienst (PIL).

**Teller:** Overige personeelskosten PIL.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan overige personeelskosten van personeel in loondienst (PIL) over de periode. De indicator wordt op organisatieniveau berekend.

Voor overige personeelskosten van personeel in loondienst (PIL) hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Overige personeelskosten betreft de indirecte personeelskosten (reiskosten, studiekosten, etc.) en personeel gerelateerde kosten (kosten werving en selectie, etc.). Het zijn alle personeel gerelateerde kosten uit de jaarrekening die niet verantwoord zijn onder salarissen en sociale kosten. Directe loonkosten en sociale kosten vallen hier niet onder (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WBedOvpWer        W.B.H    4012070    Wervingskosten|
| WBedOvpAbd        W.B.I    4012080    Arbodienst|
| WBedOvpDdd        W.B.J    4012090    Diensten door derden|
| WBedOvpZie        W.B.K    4012100    Ziekengeldverzekering|
| WBedOvpOzi        W.B.L    4012110    Ontvangen ziekengelden|
| WBedOvpDvr        W.B.M    4012120    Dotatie voorziening in verband met reorganisaties|
| WBedOvpVvr        W.B.N    4012130    Vrijval voorziening in verband met reorganisaties|
| WBedOvpDoa        W.B.O    4012140    Dotatie arbeidsongeschiktheidsvoorziening|
| WBedOvpDoj        W.B.P    4012150    Dotatie jubileumvoorziening|
| WBedOvpVva        W.B.Q    4012160    Vrijval arbeidsongeschiktheidsvoorziening|
| WBedOvpVrj        W.B.R    4012170    Vrijval jubileumvoorziening|
| WBedOvpObp        W.B.S    4012180    Overige belastingen inzake personeel|
| WBedOvpOvp        W.B.T    4012190    Overige personeelskosten niet elders genoemd|
| WBedOvpDop        W.B.U    4012990    Doorberekende overige personeelskosten|
| WBedOvpLbo        W.B.V    4012200    Loopbaanontwikkeling|

| Specificatie rubrieken Prismant|
|----------------|
|423100 Kosten werving en selectie|
|423110 Personeelsadvertenties |
|423120 Reis- en verblijfkosten sollicitanten|
|423140 Verhuiskosten |
|423190 Andere kosten werving en selectie|
|423200 Kosten opleiding en vorming |
|423210 Onderwijsmiddelen |
|423220 Tegemoetkoming studiekosten|
|423300 Kosten voorgeschreven kleding|
|423400 Gezondheidszorg |
|423410 Preventieve gezondheidszorg|
|423420 Bijdrage Arbo-dienst |
|423500 Vergoedingen |
|423510 Reiskosten |
|423520 Huursuppleties|
|423600 Kosten uitplaatsing|
|423700 Kinderopvang door derden|
|423900 Andere personeelskosten |
|423910 Geschenken bij huwelijken, geboorten, jubilea e.d. |
|423920 Kosten ontspanning personeel |

## Uitgangspunten

* Alle overige personeelskosten van alle personeelsleden in loondienst (PIL) worden geïncludeerd.
* Prismant: Lunches voor personeel tijdens bijeenkomsten e.d. worden gezien als "423900".

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van overige personeelskosten van personeel in loondienst (PIL) voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal directe loonkosten PIL (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
