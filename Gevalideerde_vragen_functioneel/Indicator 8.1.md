---
title: 8.1. Wat zijn de client cq bewonersgebonden kosten?
description: "Het totaal aan patiënt- en bewonergebonden kosten over de periode."
weight: 8
---
## Indicator

**Definitie:** Het totaal aan patiënt- en bewonergebonden kosten over de periode.

**Teller:** Niet van Toepassing.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan patiënt- en bewonergebonden kosten over de periode. Dit betreft o.a. kosten voor onderzoek, laboratorium, geneesmiddelen en hulpmiddelen (Bron: Gebruikersinstructie Kostenonderzoek 2016 - Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 23).

De indicator wordt op organisatieniveau berekend.

Voor de berekening van deze indicator hanteert de NZa het Prismant rekeningsschema (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina's 36, 37 en 38).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Prismant rekeningschema en het Referentiegrootboekschema (RGS).

| Specificatie rubrieken RGS |
|----------------|
| Niet geïmplementeerd vanuit RGS |

| Specificatie rubrieken Prismant |
|----------------|
| 461000 Kosten onderzoeksfuncties |
| 461100 Kosten beeldvormende techniek en functieonderzoek |
| 461200 Kosten laboratorium |
| 462000 Kosten behandel- en behandelingsondersteunende functies |
| 462100 Genees- en bestralingsmiddelen en dialysebenodigdheden |
| 462200 Medische gassen |
| 462300 Narcosemiddelen |
| 462400 Bloed en bloedproducten |
| 462500 Hulpmiddelen, prothesen en implantaten |
| 462600 Verband en gipsverband |
| 462700 Hechtmateriaal |
| 462800 Kosten therapieën |
| 462900 Andere kosten behandel- en behandelingsondersteunende materialen |
| 463000 Kosten van arbeidsmatige activiteiten |
| 463100 Agogie |
| 463200 Pastoraal werk |
| 463900 Andere kosten psychosociale-, behandel- en begeleidingsfuncties |
| 464000 Kosten verpleging en verzorging |
| 464100 Persoonlijke voorzieningen cliënten c.q. bewoners |
| 464200 Verplaatsingshulpmiddelen |
| 464300 Incontinentiemateriaal |
| 464900 Andere kosten verpleging en verzorging |
| 465000 Kosten niet specifiek voor onderzoek, behandeling, begeleiding, verpleging of verzorging |
| 465100 Toedienings- en afnamesystemen |
| 465200 Katheters en sondes |
| 465300 Handschoenen |
| 465400 Tandartsbenodigdheden |
| 465900 Andere kosten niet specifiek voor onderzoek, behandeling, begeleiding, verpleging of verzorging |
| 466000 Instrumentarium en apparatuur |
| 467100 Kosten ontspanning en recreatie van cliënten en bewoners |
| 467110 Ontspanningsmaterialen |
| 467120 Ontspanningsactiviteiten |
| 467200 Zakgeld voor bewoners |
| 468000 Kosten van activiteiten buiten de instelling |

**Opmerking:** de lijst hierboven betreft alle Prismant rubrieken die aanvangen met 46.

## Uitgangspunten

* Alle cliënt cq bewonersgebonden kosten worden geincludeerd.
* Binnen rekeninggroep 463 kunnen ook materiële kosten van zogenaamde therapeutische bedrijven voor bijvoorbeeld arbeids- of bezigheidstherapie verantwoord worden. Vaak kunnen de producten op prestaties van deze bedrijven doorverkocht worden aan derden. Dergelijke vergoedingen kunnen eveneens binnen de rekeninggroep 463 gecrediteerd worden, omdat het hierbij primair gaat om het elimineren van de materiële kosten van deze producten. Deze vergoedingen kunnen in feite niet meer als patiënt- of bewonergebonden kosten bestempeld worden.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van cliënt cq bewonersgebonden kosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal cliënt cq bewonersgebonden kosten (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
