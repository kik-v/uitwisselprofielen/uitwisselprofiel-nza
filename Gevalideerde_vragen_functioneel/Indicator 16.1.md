---
title: 16.1. Wat is het aantal goedgekeurde gedeclareerde verblijfsdagen?
description: "Het totaal aantal goedgekeurde gedeclareerde verblijfsdagen van cliënten met een Wlz-indicatie, ELV en/of niet-Wlz en niet-ELV."
weight: 16
---
## Indicator

**Definitie:** Het totaal aantal goedgekeurde gedeclareerde verblijfsdagen van cliënten met een Wlz-indicatie, ELV en/of niet-Wlz en niet-ELV.

**Teller:** Aantal goedgekeurde gedeclareerde verblijfsdagen van cliënten met een Wlz-indicatie, ELV en/of niet-Wlz en niet-ELV.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan goedgekeurde gedeclareerde verblijfsdagen van cliënten met een Wlz-indicatie, ELV en niet-Wlz en niet-ELV.

De indicator wordt op organisatieniveau berekend.

Onder verblijf worden alle vormen van zorg verstaan, waarbij de cliënt in de instelling overnacht. Verlofdagen (indien inzichtelijk) en verblijf zonder overnachting worden niet tot verblijf meegerekend. Niet meegenomen dienen te worden de VPT verblijfsdagen. Exclusief niet vergoedde overproductie (Bron: Gebruikersinstructie Kostenonderzoek 2016 - Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 16).

De producten die in het kader van de Wlz onder de noemer 'gedeclareerde verblijfsdag' vallen staan in het volgende Excel-bestand: 20191025_Hoofdtemplate_Gegevensuitvraag_Integrale_Vergelijking_Def_Versi.._ tabblad 'Producten Wlz (ZIN)'.

NB: Op de (deel)resultaten van deze indicator dient geen verdeelsleutel te worden toegepast. Immers, de indicator is reeds ingedeeld in Wet en sector.

## Uitgangspunten

* Met betrekking tot een 'goedgekeurde gedeclareerde verblijfsdag' wordt uitgegaan van het AW320-bericht (zie tevens: <https://www.vektis.nl/standaardisatie/standaarden/AW320-1.4>).

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer in de verslagperiode cliënten bij wie gedeclareerde verblijfsdagen zijn geregisteerd.
2. Bereken per cliënt uit stap ‘1’ het aantal gedeclareerde verblijfsdagen in de verslagperiode.
3. Bereken de indicator door het totaal aantal gedeclareerde verblijfsdagen uit stap ‘2’ bij elkaar op te tellen en te categoriseren in Wlz, ELV en overig (overig betreft niet-Wlz en niet-ELV).

Periode: dd-mm-jjjj tot en met dd-mm-jjjj
| Organisatieonderdeel | Aantal goedgekeurde gedeclareerde verblijfsdagen Wlz | Aantal goedgekeurde gedeclareerde verblijfsdagen ELV | Overig aantal goedgekeurde gedeclareerde verblijfsdagen |
|---|---|---|---|
| Totaal organisatie | Stap 3 | Stap 3 | Stap 3 |
