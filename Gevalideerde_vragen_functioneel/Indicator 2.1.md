---
title: 2.1. Aantal verloonde uren
description: "Het aantal verloonde uren van personeelsleden per jaar per financieringsstroom."
weight: 2
---
## Indicator

**Definitie:** Het aantal verloonde uren van personeelsleden per jaar per financieringsstroom.

**Teller:** Het totaal aantal verloonde uren van personeelsleden per jaar.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft per jaar per financieringsstroom het totaal aantal verloonde uren van personeelsleden. Verloonde uren betreft het aantal uren waarover het loon van de werknemer is berekend. Zie tevens: https://download.belastingdienst.nl/belastingdienst/docs/memo_verloonde_uren_lh0361t5fd.pdf

## Uitgangspunten

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer alle personeelsleden (o.b.v. de werkovereenkomst) die personeelslid waren in het betreffende jaar.
2. Bepaal per personeelslid de verloonde uren in het betreffende jaar.
3. Tel o.b.v. stap 2 alle verloonde uren bij elkaar op (teller).

| Periode:       | Verloonde uren |
|----------------|--------|
| Totaal organisatie | Stap 3 |
