---
title: 7.1. Wat zijn de algemene kosten?
description: "Het totaal aan alle algemene kosten."
weight: 7
---
## Indicator

**Definitie:** Het totaal aan alle algemene kosten.

**Teller:** Totaal aan algemene kosten.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan alle algemene kosten over de periode (Bron: Gebruikersinstructie Kostenonderzoek 2016 - Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17). Denk hierbij aan: kosten van administratie en registratie, communicatiekosten, kosten algemeen beheer, zakelijke lasten en verzekeringen, kosten audiovisuele apparatuur en andere algemene kosten.

De indicator wordt op organisatieniveau berekend.

Voor de berekening van deze indicator hanteert de NZa het Prismant rekeningsschema (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina's 35 en 36).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| -> Nader uitwerken (niet in een groep in RGS, maar verspreid) |

| Specificatie rubrieken Prismant |
|----------------|
| 451000 Kosten van administratie en registratie |
| 451100 Kantoorbenodigdheden en -apparatuur |
| 451200 Drukwerk |
| 451300 Inningskosten |
| 451400 Kosten betalingsverkeer |
| 451500 Kosten van automatisering |
| 451600 Kosten administratieve diensten door derden |
| 452000 Communicatiekosten |
| 452100 Porti |
| 452200 Telefoon |  
| 452900 Andere communicatiekosten |
| 453000 Kosten algemeen beheer |
| 453100 Accountants- en advieskosten |
| 453200 Bijdragen, lidmaatschappen en vakliteratuur |
| 453300 Dienstreizen |
| 453900 Andere kosten algemeen beheer |
| 453910 Bestuurskosten / vacatiegelden |
| 453920 Representatiekosten |
| 453930 Public Relations |
| 453990 Overige kosten algemeen beheer |
| 454000 Zakelijke lasten en verzekeringen |
| 454100 Belastingen |
| 454200 Verzekeringen en schadevergoedingen |
| 454210 Kosten verzekeringspremies |
| 454220 Betaalde schadevergoedingen |
| 454290 Ontvangen schadevergoedingen |
| 455000 Kosten audiovisuele apparatuur en benodigdheden |
| 455100 Kosten audiovisuele apparatuur en benodigdheden |
| 459000 Andere algemene kosten |
| 459100 Kosten wetenschappelijk werk |
| 459300 Kas-, voorraad- en prijsverschillen |
| 459900 Overige algemene kosten |

**Opmerking:** de lijst hierboven betreft alle Prismant rubrieken die aanvangen met 45.

## Uitgangspunten

* Alle algemene kosten worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van algemene kosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal algemene kosten (Euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
