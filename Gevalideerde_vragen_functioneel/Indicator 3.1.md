---
title: 3.1. Loonkosten personeel in loondienst
description: "Totaal aan loonkosten inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten"
weight: 3
---
## Indicator

**Definitie:** Totaal aan loonkosten inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten.

**Teller:** Loonkosten PIL.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft de loonkosten van personeel in loondienst (PIL) over de periode inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten. De indicator wordt op organisatieniveau berekend.

Voor loonkosten van personeel in loondienst (PIL) hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Loonkosten van personeel in loondienst betreft de salariskosten van in dienstbetrekking verrichte arbeid op basis van een arbeidsovereenkomst. De instelling treedt op als werkgever en is uit dien hoofde inhoudingsplichtig voor de loonbelasting en premieheffing (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 29).

Op grond van hiervan worden tot de salarissen gerekend:

* Brutosalaris (volgens inschalingtabellen) inclusief doorbetaalde salarissen tijdens ziekte;
* Vakantiebijslag;
* Vergoedingen voor overwerk, onregelmatige dienst, bereikbaarheids-, aanwezigheids- en
slaapdienst;
* Eindejaarsuitkeringen e.d. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 30).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WPerSol S.B 4002000 Sociale lasten |
| WPerLes S.A 4001000 Lonen en salarissen |

| Specificatie rubrieken Prismant*|
|----------------|
| 411000 Personeel algemene en administratieve functies |
| 411100 Management en staf algemene en administratieve functies |
| 411200 Administratief personeel |
| 411300 Automatiseringspersoneel |
| 411400 Opleidingspersoneel |
| 411500 Personeel werving |
| 411600 Personeel i.z. personeel en/of organisatie |
| 411700 Personeel algemene ondersteuning |
| 412000 Personeel hotelfuncties |
| 412100 Management civiele functies |
| 412200 Productiepersoneel |
| 412300 Distributiepersoneel |
| 412400 Personeel civiel onderhoud |
| 412500 Dienstverleningspersoneel |
| 413000 Personeel cliënt - c.q. bewonersgebonden functies |
| 413100 Management en staf patiënt- c.q. bewonergebonden functies |
| 413200 Personeel (medische) elektronica en revalidatietechniek |
| 413300 Personeel onderzoeksfuncties |
| 413400 Personeel behandel- en behandelingsondersteunende functies |
| 413500 Personeel psychosociale behandel- en begeleidingsfuncties |
| 413600 Verpleegkundig, opvoedkundig en verzorgend personeel |
| 413610 Leidinggevend verpleegkundig, opvoedkundig en verzorgend personeel |
| 413620 Niet leidinggevend verpleegkundig, opvoedkundig en verzorgend personeel |
| 413700 Personeel medische en sociaal-wetenschappelijke functies |
| 414000 Leerling personeel |
| 414100 Verpleegkundigen in opleiding (niveau 5) |
| 414101 Vierdejaars leerling-verpleegkundigen |
| 414102 Derdejaars leerling-verpleegkundigen |
| 414103 Tweedejaars leerling-verpleegkundigen |
| 414104 Eerstejaars leerling-verpleegkundigen |
| 414110 Verpleegkundigen in opleiding (niveau 4) |
| 414111 Vierdejaars leerling-verpleegkundigen |
| 414112 Derdejaars leerling-verpleegkundigen |
| 414113 Tweedejaars leerling-verpleegkundigen |
| 414114 Eerstejaars leerling-verpleegkundigen |
| 414200 Verzorgenden in opleiding (niveau 3) |
| 414202 Derdejaars leerling-verzorgenden |
| 414203 Tweedejaars leerling-verzorgenden |
| 414204 Eerstejaars leerling-verzorgenden |
| 414300 Opvoedkundig personeel in opleiding |
| 414500 Helpenden in opleiding (niveau 2) |
| 414503 Tweedejaars leerling-verzorgenden |
| 414504 Eerstejaars leerling-verzorgenden |
| 414700 Zorghulpen in opleiding (niveau 1) |
| 414800 Ander leerling-verpleegkundig, -verzorgend en -opvoedkundig personeel |
| 415000 Personeel terrein- en gebonden functies |
| |
| 416000 Andere vormen van honorering |
| 417000 Stagiaires |
| |
| 419000 Suppleties en andere kosten bij arbeidsongeschiktheid |
| 419100 Salarissen tijdens ziekte |
| 419101 Terugontvangen ziekengeld |
| 419200 Salarissen i.v.m. zwangerschapsverlof |
| 419500 Andere suppleties, gratificaties en uitkeringen |
| |
| 420000 Eindheffingen loonbelasting |
| 420100 Afdrachtvermindering loonbelasting |
| |
| 422100 Berekende sociale kosten vakantiebijslag |
| 422300 Aandeel werknemer premies sociale verzekeringen |
| 422400 Aandeel werkgever premies sociale verzekeringen |
| 422410 Korting / vrijlating basispremie WAO / werkgeversdeel Awf (Algemeen werkloosheidsfonds) |
| 422500 Ziektekostenverzekering |
| 422600 Pensioenkosten |
| 422900 Overgeboekte sociale kosten |

## Uitgangspunten

* De kosten van alle personeelsleden in loondienst (PIL) worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van loonkosten van personeel in loondienst (PIL) voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal directe loonkosten PIL (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
