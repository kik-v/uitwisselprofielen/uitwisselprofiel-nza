---
title: 5.1. Wat zijn de kosten van voeding?
description: "Totaal aan kosten van voeding."
weight: 5
---
## Indicator

**Definitie:** Totaal aan kosten van voeding.

**Teller:** Totaal aan kosten van voeding.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft de materiële kosten die verband houden met de bereiding en verstrekking van maaltijden en andere voedingsproducten. De indicator wordt op organisatieniveau berekend (Bron: Gebruikersinstructie Kosten onderzoek 2016 - Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Voor de berekening van deze indicator hanteert de NZa het Prismant rekeningsschema (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 33).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Prismant rekeningschema en het Referentiegrootboekschema (RGS).

| Specificatie rubrieken RGS |
|----------------|
| Niet geïmplementeerd vanuit RGS |

| Specificatie rubrieken Prismant |
|----------------|
| 431000 Voedingsmiddelen |
| 431100 Ingrediënten en producten |
| 431200 Maaltijden bereid door derden |
| 431300 Bijzondere voedingen en producten |
| 432000 Keuken- en restauratieve apparatuur en benodigdheden |
| 433000 Voedingsgeld voor bewoners en afdelingen |

## Uitgangspunten

* De materiële kosten van alle voeding worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van voeding voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal kosten voeding (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
