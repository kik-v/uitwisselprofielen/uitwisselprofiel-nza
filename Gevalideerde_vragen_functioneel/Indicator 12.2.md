---
title: 12.2. Wat zijn de berekende correcties op het toegekend wettelijk budget? 
description: "In deze rekeninggroep kunnen de correcties op het budget volgens rekeninggroep 811 worden verantwoord. Deze correcties zijn bijvoorbeeld via NZa-circulaires reeds bekend, maar ze zijn nog niet door middel van een rekenstaat bevestigd."
weight: 12
---
## Indicator

**Definitie:** In deze rekeninggroep kunnen de correcties op het budget volgens rekeninggroep 811 worden verantwoord. Deze correcties zijn bijvoorbeeld via NZa-circulaires reeds bekend, maar ze zijn nog niet door middel van een rekenstaat bevestigd. Generieke aanpassingen betreffen aanpassingen die in principe voor alle soortgelijke instellingen gelden. Incidentele aanpassingen hebben betrekking op een wijziging van het budget voor een bepaalde instelling.

**Teller:** De berekende correcties op het toegekend wettelijk budget.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft de berekende correcties op het toegekend wettelijk budget. De indicator wordt op organisatieniveau berekend.

In deze rekeninggroep kunnen de correcties op het budget volgens rekeninggroep 811 worden verantwoord. Deze correcties zijn bijvoorbeeld via NZa-circulaires reeds bekend, maar ze zijn nog niet door middel van een rekenstaat bevestigd.

Generieke aanpassingen betreffen aanpassingen die in principe voor alle soortgelijke instellingen gelden. Incidentele aanpassingen hebben betrekking op een wijziging van het budget voor een bepaalde instelling. (bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG- versie 12 september 2017, pagina 16).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| 812000 Berekende correcties op het toegekend wettelijk budget  |

## Uitgangspunten

* Geen?

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van de berekende correcties op het toegekende wettelijk budget voor de betreffende meetperiode bij elkaar op.

| Organisatieonderdeel       | Totaal berekende correcties op het toegekend wettelijk budget (euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
