---
title: 10.2. Wat zijn de kosten voor vervoersmiddelen en immateriele vaste activa?
description: "Het totaal aan kosten voor vervoersmiddelen en immateriele vaste activa."
weight: 10
---
## Indicator

**Definitie:** Het totaal aan kosten voor vervoersmiddelen en immateriele vaste activa.

**Teller:** Totaal aan kosten voor vervoersmiddelen en immateriele vaste activa.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan kosten voor vervoersmiddelen en immateriele vaste activa over de periode. De indicator wordt op organisatieniveau berekend.

Voor de kosten voor vervoersmiddelen en immateriele vaste activa hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema zijn dit de vervoersmiddelen en immateriele vaste activa die vallen onder rubriek 48 (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004, Publicatienummer 203.25, pagina 38 en 39)

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| 480012 Afschrijvingskosten aanloopkosten (WZV-vergunningsplichtig) |
| 480022 Afschrijvingskosten aanloopkosten (WTG-gefinancierd) |
| 480032 Afschrijvingskosten aanloopkosten (niet WTG-gefinancierd) |
| 480112 Afschrijvingskosten aanloopverliezen (WZV-vergunningsplichtig) |
| 480122 Afschrijvingskosten aanloopverliezen (WTG-gefinancierd) |
| 480132 Afschrijvingskosten aanloopverliezen (niet WTG-gefinancierd) |
| 480532 Afschrijvingskosten goodwill |
| 480632 Afschrijvingskosten licentiekosten |
| 480932 Afschrijvingskosten overige te verrekenen verliezen |
| 484222 Afschrijvingskosten vervoersmiddelen (WTG gefinancierd) |
| 484232 Afschrijvingskosten vervoersmiddelen (niet WTG gefinancierd) |
| 486725 Huur en leasing vervoersmiddelen (WTG gefinancierd) |
| 486735 Huur en leasing vervoersmiddelen (niet WTG gefinancierd) |

## Uitgangspunten

* Alle de kosten voor vervoersmiddelen en immateriele vaste activa worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van de kosten voor vervoersmiddelen en immateriele vaste activa voor de betreffende meetperiode bij elkaar op.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj

| Organisatieonderdeel       | Totaal aan kosten voor vervoersmiddelen en immateriele vaste activa (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
