---
title: 11.1. Wat zijn de kosten en opbrengsten voor hulpbedrijven en overboekingsrekening?
description: "Het totaal aan kosten en opbrengsten voor hulpbedrijven en overboekingsrekening."
weight: 11
---
## Indicator

**Definitie:** Het totaal aan kosten en opbrengsten voor hulpbedrijven en overboekingsrekening.

**Teller:** Totaal aan kosten en opbrengsten voor hulpbedrijven en overboekingsrekening.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan kosten en opbrengsten voor intercompany doorbelastingen en overboekingen over de periode. De aanname is dat de kosten en opbrengsten voor intercompany diensten op totaalniveau (of u deze binnen de organisatie nou centraal of decentraal geboekt worden) (grotendeels) tegen elkaar wegvallen en dat de kosten/opbrengsten binnen deze kostensoort daarom nihil zullen zijn. Indien u de opbrengsten in uw administratie echter niet als negatieve kosten boekt, dan wordt u daarnaast gevraagd dit aan te geven bij de correctieposten (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 23).  

De indicator wordt op organisatieniveau berekend.

Voor het totaal aan kosten en opbrengsten voor intercompany doorbelastingen en overboekingen hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema betreft dit rubriek 49 (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 40 en 41).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| -> Nader uitwerken (op dit moment onduidelijk) |

| Specificatie rubrieken Prismant|
|----------------|
| 491000 Hulpbedrijven |
| 499000 Overboekingsrekening |

## Uitgangspunten

* Alle kosten en opbrengsten voor intercompany doorbelastingen en overboekingen worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van kosten en opbrengsten voor intercompany doorbelastingen en overboekingen voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal kosten en opbrengsten voor intercompany doorbelastingen en overboekingen (Euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
