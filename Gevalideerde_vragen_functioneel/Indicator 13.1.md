---
title: 13.1. Wat zijn de overige bedrijfsopbrengsten? 
description: "Opbrengsten buiten wettelijk toegekend budget (ook wel alle opbrengsten die niet kunnen worden ondergebracht in één van de overige categorieën). Denk hierbij aan zorgprestaties tussen instellingen, zorgprestaties derde compartiment, subsidies (met uitzondering van loonkostensubsidies, verkoop vastgoed."
weight: 13
---
## Indicator

**Definitie:** Opbrengsten buiten wettelijk toegekend budget (ook wel alle opbrengsten die niet kunnen worden ondergebracht in één van de overige categorieën). Denk hierbij aan zorgprestaties tussen instellingen, zorgprestaties derde compartiment, subsidies (met uitzondering van loonkostensubsidies, verkoop vastgoed.

**Teller:** Overige bedrijfsopbrengsten

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan overige bedrijfsopbrengsten.

De indicator wordt op organisatieniveau berekend.

Voor de overige bedrijfsopbrengsten hanteert de NZa de definitie volgens het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Opbrengsten buiten wettelijk toegekend budget (ook wel alle opbrengsten die niet kunnen worden ondergebracht in één van de overige categorieën). Denk hierbij aan zorgprestaties tussen instellingen, zorgprestaties derde compartiment, subsidies (met uitzondering van loonkostensubsidies, verkoop vastgoed.

In het Prismant rekeningschema zijn dit alle rekeningen uit rubrieken 82 en 83. Exclusief 822000 'Eigen bijdragen cliënten'. Deze wordt apart uitgevraagd (zie indicator 13.2).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant|
|----------------|
| 821000 Subsidies |
| 825000 Zorgprestaties tussen instellingen |
| 826000 Zorgprestaties derde compartiment |
| 827000 Overige zorgprestaties |
| 828000 Overige dienstverlening |
| 829000 Overige opbrengsten |
| 831000 Opbrengsten algemene en administratieve diensten |
| 832000 Opbrengsten maaltijden en andere consumpties |
| 833000 Opbrengsten andere hotelmatige diensten |
| 835000 Opbrensten technische en/ of agrarische diensten |

## Uitgangspunten

* Eigen bijdrage cliënten wordt niet geïncludeerd. Die wordt uitgevraagd in indicator 13.2.
Let op: Indien dit minder dan 2% van de totale opbrengsten betreft kan de invuller er ook voor kiezen om dit WEL mee te nemen bij overige bedrijfsopbrengsten.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van 'Overige opbrengsten' bij elkaar op.

| Periode:       | Totaal Overige opbrengsten (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
