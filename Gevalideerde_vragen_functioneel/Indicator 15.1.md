---
title: 15.1 Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)? 
description: "De opbrengsten uit gewone bedrijfsuitvoering, dat wil zeggen de opbrengsten uit exploitatie van de zorginstelling. Dit betreft niet alle overige baten van de rechtspersoon die feitelijk niets met de zorginstelling en de doelstelling daarvan te maken hebben."
weight: 15
---
## Indicator

**Definitie:** De opbrengsten uit gewone bedrijfsuitvoering, dat wil zeggen de opbrengsten uit exploitatie van de zorginstelling. Dit betreft niet alle overige baten van de rechtspersoon die feitelijk niets met de zorginstelling en de doelstelling daarvan te maken hebben.

**Teller:** Opbrengsten uit gewone bedrijfsuitoefening.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft de opbrengsten uit gewone bedrijfsvoering. De indicator wordt op organisatieniveau berekend.

Voor de opbrengsten uit gewone bedrijfsuitoefening hanteert de NZa de definitie volgens het Prismant rekeningsschema.

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken |

| Specificatie rubrieken Prismant |
|----------------|
| 811000 Toegekend wettelijk budget |
| 821000 Subsidies |
| 822000 Eigen bijdragen cliënten |
| 825000 Zorgprestaties tussen instellingen |
| 826000 Zorgprestaties derde compartiment |
| 827000 Overige zorgprestaties |
| 828000 Overige dienstverlening |
| 829000 Overige opbrengsten |
| 831000 Opbrengsten algemene en administratieve diensten |
| 832000 Opbrengsten maaltijden en andere consumpties |
| 833000 Opbrengsten andere hotelmatige diensten |
| 835000 Opbrengsten technische en / of agrarische diensten |

**Opmerking:** de lijst hierboven betreft alle Prismant rubrieken die aanvangen met 8 behoudens 891000 "Correctie budgetafrekeningen voorgaande boekjaren".

## Uitgangspunten

* Geen

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van de opbrengsten uit gewone bedrijfsuitoefening voor de betreffende meetperiode bij elkaar op.

| Organisatieonderdeel       | Totaal Opbrengsten uit gewone bedrijfsuitoefening (euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
