---
title: 10.3. Wat zijn huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties?
description: "Het totaal aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties."
weight: 10
---
## Indicator

**Definitie:** Het totaal aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties.

**Teller:** Totaal kosten huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties over de periode. De indicator wordt op organisatieniveau berekend.

Voor huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema zijn dit de huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties die vallen onder rubriek 48 (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 38 en 39).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| 481212 Afschrijvingskosten terreinvoorzieningen (WZV vergunningsplichtig) |
| 481222 Afschrijvingskosten terreinvoorzieningen (WTG gefinancierd) |
| 481232 Afschrijvingskosten terreinvoorzieningen (niet WZV/WTG gefinancierd) |
| 481242 Afschrijvingskosten terreinvoorzieningen (Trekkingsrechten / WZV meldingsplichtig) |
| 481252 Afschrijvingskosten terreinvoorzieningen (Instandhouding / WZV meldingsplichtig) |
| 482112 Afschrijvingskosten gebouwen (WZV vergunningsplichtig) |
| 482122 Afschrijvingskosten gebouwen (WTG gefinancierd) |
| 482132 Afschrijvingskosten gebouwen (niet WZV/WTG gefinancierd) |
| 482142 Afschrijvingskosten gebouwen (Trekkingsrechten / WZV meldingsplichtig) |
| 482152 Afschrijvingskosten gebouwen (Instandhouding / WZV meldingsplichtig) |
| 482212 Afschrijvingskosten verbouwingen (WZV vergunningsplichtig) |
| 482222 Afschrijvingskosten verbouwingen (WTG gefinancierd) |
| 482232 Afschrijvingskosten verbouwingen (niet WZV/WTG gefinancierd) |
| 482242 Afschrijvingskosten verbouwingen (Trekkingsrechten / WZV meldingsplichtig) |
| 482252 Afschrijvingskosten verbouwingen (Instandhouding / WZV meldingsplichtig) |
| 482312 Afschrijvingskosten semi permanente gebouwen (WZV vergunningsplichtig) |
| 482322 Afschrijvingskosten semi permanente gebouwen (WTG gefinancierd) |
| 482332 Afschrijvingskosten semi permanente gebouwen (niet WZV/WTG gefinancierd) |
| 482342 Afschrijvingskosten semi permanente gebouwen (Trekkingsrechten / WZV meldingsplichtig) |
| 482352 Afschrijvingskosten semi permanente gebouwen (Instandhouding / WZV meldingsplichtig) |
| 482412 Afschrijvingskosten houten paviljoens (WZV vergunningsplichtig) |
| 482422 Afschrijvingskosten houten paviljoens (WTG gefinancierd) |
| 482432 Afschrijvingskosten houten paviljoens (niet WZV/WTG gefinancierd) |
| 482442 Afschrijvingskosten houten paviljoens (Trekkingsrechten / WZV meldingsplichtig) |
| 482452 Afschrijvingskosten houten paviljoens (Instandhouding / WZV meldingsplichtig) |
| 483112 Afschrijvingskosten installaties (WZV vergunningsplichtig) |
| 483122 Afschrijvingskosten installaties (WTG gefinancierd) |
| 483132 Afschrijvingskosten installaties (niet WZV/WTG gefinancierd) |
| 483142 Afschrijvingskosten installaties (Trekkingsrechten / WZV meldingsplichtig) |
| 483152 Afschrijvingskosten installaties (Instandhouding / WZV meldingsplichtig) |
| 484112 Afschrijvingskosten inventaris (WZV vergunningsplichtig art.2 WBMV) |
| 484122 Afschrijvingskosten inventaris (WTG gefinancierd) |
| 484132 Afschrijvingskosten inventaris (niet WTG gefinancierd) |
| 484222 Afschrijvingskosten vervoersmiddelen (WTG gefinancierd) |
| 484232 Afschrijvingskosten vervoersmiddelen (niet WTG gefinancierd) |
| 484322 Afschrijvingskosten automatisering (WTG gefinancierd) |
| 484332 Afschrijvingskosten automatisering (niet WTG gefinancierd) |
| 485000 Interest |
| 485100 Interest langlopende schulden |
| 485190 Egalisatierekening interest |
| 485205 Interest leasingcontracten |
| 485211 Afschrijving disagio op leningen |
| 485300 Interest kortlopende schulden |
| 485311 Afschrijving emissie- en leningskosten |
| 485412 Afschrijving betaalde boeterente bij vervroegde aflossing |
| 485900 Ontvangen interest |
| 486115 Huur en leasing terreinvoorzieningen (WZV vergunningsplichtig) |
| 486125 Huur en leasing terreinvoorzieningen (WTG gefinancierd) |
| 486135 Huur en leasing terreinvoorzieningen (niet WZV/WTG gefinancierd) |
| 486215 Huur en leasing gebouwen (WZV vergunningsplichtig) |
| 486225 Huur en leasing gebouwen (WTG gefinancierd) |
| 486235 Huur en leasing gebouwen (niet WZV/WTG gefinancierd) |
| 486315 Huur en leasing semi permanente gebouwen (WZV vergunningsplichtig) |
| 486325 Huur en leasing semi permanente gebouwen (WTG gefinancierd) |
| 486335 Huur en leasing semi permanente gebouwen (niet WZV/WTG gefinancierd) |
| 486415 Huur en leasing houten paviljoens (WZV vergunningsplichtig) |
| 486425 Huur en leasing houten paviljoens (WTG gefinancierd) |
| 486435 Huur en leasing houten paviljoens (niet WZV/WTG gefinancierd) |
| 486515 Huur en leasing installaties (WZV vergunningsplichtig) |
| 486525 Huur en leasing installaties (WTG gefinancierd) |
| 486535 Huur en leasing installaties (niet WZV/WTG gefinancierd) |
| 486615 Huur en leasing inventaris (WZV vergunningsplichtig art.2 WBMV) |
| 486625 Huur en leasing inventaris (WTG gefinancierd) |
| 486635 Huur en leasing inventaris (niet WTG gefinancierd) |
| 486825 Huur en leasing automatisering (WTG gefinancierd) |
| 486835 Huur en leasing automatisering (niet WTG gefinancierd) |
| 489150 Dotaties egalisatierekeningafschrijvingen instandhoudingsinvesteringen |
| 489211 Dotatie aan voorziening dubieuze vorderingen |
| 489212 Afboeking dubieuze vorderingen |
| 489221 Dotatie voorziening incourante voorraden |
| 489222 Afboeking incourante voorraden |
| 489230 Bestemmingsreserve huisvesting (voorheen Vernieuwingsfonds) |

## Uitgangspunten

* Alle huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen voor kosten huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties voor de betreffende meetperiode bij elkaar op.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj

| Organisatieonderdeel       | Totaal kosten aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
